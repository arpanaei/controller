// Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

syntax = "proto3";
package context;

import "kpi_sample_types.proto";

service ContextService {
  rpc ListContextIds     (Empty       ) returns (       ContextIdList   ) {}
  rpc ListContexts       (Empty       ) returns (       ContextList     ) {}
  rpc GetContext         (ContextId   ) returns (       Context         ) {}
  rpc SetContext         (Context     ) returns (       ContextId       ) {}
  rpc RemoveContext      (ContextId   ) returns (       Empty           ) {}
  rpc GetContextEvents   (Empty       ) returns (stream ContextEvent    ) {}

  rpc ListTopologyIds    (ContextId   ) returns (       TopologyIdList  ) {}
  rpc ListTopologies     (ContextId   ) returns (       TopologyList    ) {}
  rpc GetTopology        (TopologyId  ) returns (       Topology        ) {}
  rpc SetTopology        (Topology    ) returns (       TopologyId      ) {}
  rpc RemoveTopology     (TopologyId  ) returns (       Empty           ) {}
  rpc GetTopologyEvents  (Empty       ) returns (stream TopologyEvent   ) {}

  rpc ListDeviceIds      (Empty       ) returns (       DeviceIdList    ) {}
  rpc ListDevices        (Empty       ) returns (       DeviceList      ) {}
  rpc GetDevice          (DeviceId    ) returns (       Device          ) {}
  rpc SetDevice          (Device      ) returns (       DeviceId        ) {}
  rpc RemoveDevice       (DeviceId    ) returns (       Empty           ) {}
  rpc GetDeviceEvents    (Empty       ) returns (stream DeviceEvent     ) {}

  rpc ListLinkIds        (Empty       ) returns (       LinkIdList      ) {}
  rpc ListLinks          (Empty       ) returns (       LinkList        ) {}
  rpc GetLink            (LinkId      ) returns (       Link            ) {}
  rpc SetLink            (Link        ) returns (       LinkId          ) {}
  rpc RemoveLink         (LinkId      ) returns (       Empty           ) {}
  rpc GetLinkEvents      (Empty       ) returns (stream LinkEvent       ) {}

  rpc ListServiceIds     (ContextId   ) returns (       ServiceIdList   ) {}
  rpc ListServices       (ContextId   ) returns (       ServiceList     ) {}
  rpc GetService         (ServiceId   ) returns (       Service         ) {}
  rpc SetService         (Service     ) returns (       ServiceId       ) {}
  rpc RemoveService      (ServiceId   ) returns (       Empty           ) {}
  rpc GetServiceEvents   (Empty       ) returns (stream ServiceEvent    ) {}

  rpc ListConnectionIds  (ServiceId   ) returns (       ConnectionIdList) {}
  rpc ListConnections    (ServiceId   ) returns (       ConnectionList  ) {}
  rpc GetConnection      (ConnectionId) returns (       Connection      ) {}
  rpc SetConnection      (Connection  ) returns (       ConnectionId    ) {}
  rpc RemoveConnection   (ConnectionId) returns (       Empty           ) {}
  rpc GetConnectionEvents(Empty       ) returns (stream ConnectionEvent ) {}
}

// ----- Generic -------------------------------------------------------------------------------------------------------
message Empty {}

message Uuid {
  string uuid = 1;
}

enum EventTypeEnum {
  EVENTTYPE_UNDEFINED = 0;
  EVENTTYPE_CREATE = 1;
  EVENTTYPE_UPDATE = 2;
  EVENTTYPE_REMOVE = 3;
}

message Event {
  double timestamp = 1;
  EventTypeEnum event_type = 2;
}

// ----- Context -------------------------------------------------------------------------------------------------------
message ContextId {
  Uuid context_uuid = 1;
}

message Context {
  ContextId context_id = 1;
  repeated TopologyId topology_ids = 2;
  repeated ServiceId service_ids = 3;
  TeraFlowController controller = 4;
}

message ContextIdList {
  repeated ContextId context_ids = 1;
}

message ContextList {
  repeated Context contexts = 1;
}

message ContextEvent {
  Event event = 1;
  ContextId context_id = 2;
}


// ----- Topology ------------------------------------------------------------------------------------------------------
message TopologyId {
  ContextId context_id = 1;
  Uuid topology_uuid = 2;
}

message Topology {
  TopologyId topology_id = 1;
  repeated DeviceId device_ids = 2;
  repeated LinkId link_ids = 3;
}

message TopologyIdList {
  repeated TopologyId topology_ids = 1;
}

message TopologyList {
  repeated Topology topologies = 1;
}

message TopologyEvent {
  Event event = 1;
  TopologyId topology_id = 2;
}


// ----- Device --------------------------------------------------------------------------------------------------------
message DeviceId {
  Uuid device_uuid = 1;
}

message Device {
  DeviceId device_id = 1;
  string device_type = 2;
  DeviceConfig device_config = 3;
  DeviceOperationalStatusEnum device_operational_status = 4;
  repeated DeviceDriverEnum device_drivers = 5;
  repeated EndPoint device_endpoints = 6;
}

message DeviceConfig {
  repeated ConfigRule config_rules = 1;
}

enum DeviceDriverEnum {
  DEVICEDRIVER_UNDEFINED = 0; // also used for emulated
  DEVICEDRIVER_OPENCONFIG = 1;
  DEVICEDRIVER_TRANSPORT_API = 2;
  DEVICEDRIVER_P4 = 3;
  DEVICEDRIVER_IETF_NETWORK_TOPOLOGY = 4;
  DEVICEDRIVER_ONF_TR_352 = 5;
}

enum DeviceOperationalStatusEnum {
  DEVICEOPERATIONALSTATUS_UNDEFINED = 0;
  DEVICEOPERATIONALSTATUS_DISABLED = 1;
  DEVICEOPERATIONALSTATUS_ENABLED = 2;
}

message DeviceIdList {
  repeated DeviceId device_ids = 1;
}

message DeviceList {
  repeated Device devices = 1;
}

message DeviceEvent {
  Event event = 1;
  DeviceId device_id = 2;
}


// ----- Link ----------------------------------------------------------------------------------------------------------
message LinkId {
  Uuid link_uuid = 1;
}

message Link {
  LinkId link_id = 1;
  repeated EndPointId link_endpoint_ids = 2;
}

message LinkIdList {
  repeated LinkId link_ids = 1;
}

message LinkList {
  repeated Link links = 1;
}

message LinkEvent {
  Event event = 1;
  LinkId link_id = 2;
}


// ----- Service -------------------------------------------------------------------------------------------------------
message ServiceId {
  ContextId context_id = 1;
  Uuid service_uuid = 2;
}

message Service {
  ServiceId service_id = 1;
  ServiceTypeEnum service_type = 2;
  repeated EndPointId service_endpoint_ids = 3;
  repeated Constraint service_constraints = 4;
  ServiceStatus service_status = 5;
  ServiceConfig service_config = 6;
}

enum ServiceTypeEnum {
  SERVICETYPE_UNKNOWN = 0;
  SERVICETYPE_L3NM = 1;
  SERVICETYPE_L2NM = 2;
  SERVICETYPE_TAPI_CONNECTIVITY_SERVICE = 3;
}

enum ServiceStatusEnum {
  SERVICESTATUS_UNDEFINED = 0;
  SERVICESTATUS_PLANNED = 1;
  SERVICESTATUS_ACTIVE =  2;
  SERVICESTATUS_PENDING_REMOVAL = 3;
}

message ServiceStatus {
  ServiceStatusEnum service_status = 1;
}

message ServiceConfig {
  repeated ConfigRule config_rules = 1;
}

message ServiceIdList {
  repeated ServiceId service_ids = 1;
}

message ServiceList {
  repeated Service services = 1;
}

message ServiceEvent {
  Event event = 1;
  ServiceId service_id = 2;
}


// ----- Connection ----------------------------------------------------------------------------------------------------
message ConnectionId {
  Uuid connection_uuid = 1;
}

message Connection {
  ConnectionId connection_id = 1;
  ServiceId service_id = 2;
  repeated EndPointId path_hops_endpoint_ids = 3;
  repeated ServiceId sub_service_ids = 4;
}

message ConnectionIdList {
  repeated ConnectionId connection_ids = 1;
}

message ConnectionList {
  repeated Connection connections = 1;
}

message ConnectionEvent {
  Event event = 1;
  ConnectionId connection_id = 2;
}


// ----- Endpoint ------------------------------------------------------------------------------------------------------
message EndPointId {
  TopologyId topology_id = 1;
  DeviceId device_id = 2;
  Uuid endpoint_uuid = 3;
}

message EndPoint {
  EndPointId endpoint_id = 1;
  string endpoint_type = 2;
  repeated kpi_sample_types.KpiSampleType kpi_sample_types = 3;
}


// ----- Configuration -------------------------------------------------------------------------------------------------
enum ConfigActionEnum {
  CONFIGACTION_UNDEFINED = 0;
  CONFIGACTION_SET = 1;
  CONFIGACTION_DELETE = 2;
}

message ConfigRule {
  ConfigActionEnum action = 1;
  string resource_key = 2;
  string resource_value = 3;
}


// ----- Constraint ----------------------------------------------------------------------------------------------------
message Constraint {
  string constraint_type = 1;
  string constraint_value = 2;
}


// ----- Miscellaneous -------------------------------------------------------------------------------------------------
message TeraFlowController {
  ContextId context_id = 1;
  string ip_address = 2;
  uint32 port = 3;
}

message AuthenticationResult {
  ContextId context_id = 1;
  bool authenticated = 2;
}
