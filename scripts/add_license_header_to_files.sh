#!/bin/bash
# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


docker pull ghcr.io/google/addlicense:latest
docker run -it -v ${PWD}:/src ghcr.io/google/addlicense \
    -l apache -c "H2020 TeraFlow (https://www.teraflow-h2020.eu/)" -y 2021-2023 \
    -ignore "data/*" -ignore "data/**" -ignore "tmp/*" -ignore "tmp/**" -ignore "manifests/cttc-ols/*" \
    -ignore "coverage/*" -ignore "coverage/**" -ignore ".vscode/*" -ignore ".vscode/**" \
    -ignore ".git/*" -ignore ".git/**" -ignore "proto/uml/*" -ignore "proto/uml/**" \
    -ignore "src/**/__pycache__/*" -ignore "src/**/__pycache__/**" \
    -ignore "src/.benchmarks/*" -ignore "src/.benchmarks/**" -ignore ".benchmarks/*" -ignore ".benchmarks/**" \
    -ignore "src/.pytest_cache/*" -ignore "src/.pytest_cache/**" -ignore ".pytest_cache/*" -ignore ".pytest_cache/**" \
    -ignore "src/**/target/generated-sources/grpc/*" -ignore "src/**/target/generated-sources/grpc/**" \
    -ignore "src/**/*_pb2.py" -ignore "src/**/*_pb2_grpc.py" \
    -ignore "src/device/service/drivers/openconfig/templates/**/*.xml" \
    -ignore "src/dlt/*" -ignore "src/dlt/**" \
    -ignore "src/**/.mvn/*" -ignore "src/**/.mvn/**" \
    *
