#!/usr/bin/env bash
# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Make folder containing the script the root folder for its execution
cd $(dirname $0)

echo "BUILD context"
context/genproto.sh
docker build -t "context:develop" -f context/Dockerfile --quiet .
docker build -t "context:test" -f context/tests/Dockerfile --quiet .

cd monitoring
./genproto.sh
cd ..

echo "BUILD monitoring"
docker build -t "monitoring:dockerfile" -f monitoring/Dockerfile .

echo "Prune unused images"
docker image prune --force
