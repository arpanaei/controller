# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import functools
from typing import Any, List, Union

ACTION_MSG_SET_ENDPOINT      = 'Set EndPoint(device_uuid={:s}, endpoint_uuid={:s}, topology_uuid={:s})'
ACTION_MSG_DELETE_ENDPOINT   = 'Delete EndPoint(device_uuid={:s}, endpoint_uuid={:s}, topology_uuid={:s})'

ACTION_MSG_SET_CONSTRAINT    = 'Set Constraint(constraint_type={:s}, constraint_value={:s})'
ACTION_MSG_DELETE_CONSTRAINT = 'Delete Constraint(constraint_type={:s}, constraint_value={:s})'

ACTION_MSG_SET_CONFIG        = 'Set Resource(key={:s}, value={:s})'
ACTION_MSG_DELETE_CONFIG     = 'Delete Resource(key={:s}, value={:s})'

def _check_errors(
        message : str, parameters_list : List[Any], results_list : List[Union[bool, Exception]]
    ) -> List[str]:
    errors = []
    for parameters, results in zip(parameters_list, results_list):
        if not isinstance(results, Exception): continue
        message = message.format(*tuple(map(str, parameters)))
        errors.append('Unable to {:s}; error({:s})'.format(message, str(results)))
    return errors

check_errors_setendpoint      = functools.partial(_check_errors, ACTION_MSG_SET_ENDPOINT     )
check_errors_deleteendpoint   = functools.partial(_check_errors, ACTION_MSG_DELETE_ENDPOINT  )
check_errors_setconstraint    = functools.partial(_check_errors, ACTION_MSG_SET_CONSTRAINT   )
check_errors_deleteconstraint = functools.partial(_check_errors, ACTION_MSG_DELETE_CONSTRAINT)
check_errors_setconfig        = functools.partial(_check_errors, ACTION_MSG_SET_CONFIG       )
check_errors_deleteconfig     = functools.partial(_check_errors, ACTION_MSG_DELETE_CONFIG    )
