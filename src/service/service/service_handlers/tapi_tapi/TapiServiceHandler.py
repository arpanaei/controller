# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import anytree, json, logging
from typing import Any, Dict, List, Optional, Tuple, Union
from common.orm.Database import Database
from common.orm.HighLevel import get_object
from common.orm.backend.Tools import key_to_str
from common.type_checkers.Checkers import chk_type
from context.client.ContextClient import ContextClient
from device.client.DeviceClient import DeviceClient
from device.proto.context_pb2 import Device
from service.service.database.ConfigModel import ORM_ConfigActionEnum, get_config_rules
from service.service.database.ContextModel import ContextModel
from service.service.database.DeviceModel import DeviceModel
from service.service.database.ServiceModel import ServiceModel
from service.service.service_handler_api._ServiceHandler import _ServiceHandler
from service.service.service_handler_api.AnyTreeTools import TreeNode, delete_subnode, get_subnode, set_subnode_value
from service.service.service_handlers.Tools import config_rule_set, config_rule_delete

LOGGER = logging.getLogger(__name__)

class TapiServiceHandler(_ServiceHandler):
    def __init__(   # pylint: disable=super-init-not-called
        self, db_service : ServiceModel, database : Database, context_client : ContextClient,
        device_client : DeviceClient, **settings
    ) -> None:
        self.__db_service = db_service
        self.__database = database
        self.__context_client = context_client # pylint: disable=unused-private-member
        self.__device_client = device_client

        self.__db_context : ContextModel = get_object(self.__database, ContextModel, self.__db_service.context_fk)
        str_service_key = key_to_str([self.__db_context.context_uuid, self.__db_service.service_uuid])
        db_config = get_config_rules(self.__database, str_service_key, 'running')
        self.__resolver = anytree.Resolver(pathattr='name')
        self.__config = TreeNode('.')
        for action, resource_key, resource_value in db_config:
            if action == ORM_ConfigActionEnum.SET:
                try:
                    resource_value = json.loads(resource_value)
                except: # pylint: disable=bare-except
                    pass
                set_subnode_value(self.__resolver, self.__config, resource_key, resource_value)
            elif action == ORM_ConfigActionEnum.DELETE:
                delete_subnode(self.__resolver, self.__config, resource_key)

    def SetEndpoint(self, endpoints : List[Tuple[str, str, Optional[str]]]) -> List[Union[bool, Exception]]:
        chk_type('endpoints', endpoints, list)
        if len(endpoints) != 2: return []

        service_uuid = self.__db_service.service_uuid
        service_settings : TreeNode = get_subnode(self.__resolver, self.__config, 'settings', None)
        if service_settings is None: raise Exception('Unable to settings for Service({:s})'.format(str(service_uuid)))

        json_settings : Dict = service_settings.value
        capacity_value   = json_settings.get('capacity_value',   1)
        capacity_unit    = json_settings.get('capacity_unit',    'GHz')
        layer_proto_name = json_settings.get('layer_proto_name', 'PHOTONIC_MEDIA')
        layer_proto_qual = json_settings.get('layer_proto_qual', 'tapi-photonic-media:PHOTONIC_LAYER_QUALIFIER_NMC')
        direction        = json_settings.get('direction',        'UNIDIRECTIONAL')

        results = []
        try:
            device_uuid = endpoints[0][0]
            db_device : DeviceModel = get_object(self.__database, DeviceModel, device_uuid, raise_if_not_found=True)
            json_device = db_device.dump(include_config_rules=False, include_drivers=True, include_endpoints=True)
            json_device_config : Dict = json_device.setdefault('device_config', {})
            json_device_config_rules : List = json_device_config.setdefault('config_rules', [])
            json_device_config_rules.extend([
                config_rule_set('/service[{:s}]'.format(service_uuid), {
                    'uuid'                    : service_uuid,
                    'input_sip'               : endpoints[0][1],
                    'output_sip'              : endpoints[1][1],
                    'capacity_unit'           : capacity_unit,
                    'capacity_value'          : capacity_value,
                    'layer_protocol_name'     : layer_proto_name,
                    'layer_protocol_qualifier': layer_proto_qual,
                    'direction'               : direction,
                }),
            ])
            self.__device_client.ConfigureDevice(Device(**json_device))
            results.append(True)
        except Exception as e: # pylint: disable=broad-except
            LOGGER.exception('Unable to SetEndpoint for Service({:s})'.format(str(service_uuid)))
            results.append(e)

        return results

    def DeleteEndpoint(self, endpoints : List[Tuple[str, str, Optional[str]]]) -> List[Union[bool, Exception]]:
        chk_type('endpoints', endpoints, list)
        if len(endpoints) != 2: return []

        service_uuid = self.__db_service.service_uuid
        results = []
        try:
            device_uuid = endpoints[0][0]
            db_device : DeviceModel = get_object(self.__database, DeviceModel, device_uuid, raise_if_not_found=True)
            json_device = db_device.dump(include_config_rules=False, include_drivers=True, include_endpoints=True)
            json_device_config : Dict = json_device.setdefault('device_config', {})
            json_device_config_rules : List = json_device_config.setdefault('config_rules', [])
            json_device_config_rules.extend([
                config_rule_delete('/service[{:s}]'.format(service_uuid), {'uuid': service_uuid})
            ])
            self.__device_client.ConfigureDevice(Device(**json_device))
            results.append(True)
        except Exception as e: # pylint: disable=broad-except
            LOGGER.exception('Unable to DeleteEndpoint for Service({:s})'.format(str(service_uuid)))
            results.append(e)

        return results

    def SetConstraint(self, constraints : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('constraints', constraints, list)
        if len(constraints) == 0: return []

        msg = '[SetConstraint] Method not implemented. Constraints({:s}) are being ignored.'
        LOGGER.warning(msg.format(str(constraints)))
        return [True for _ in range(len(constraints))]

    def DeleteConstraint(self, constraints : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('constraints', constraints, list)
        if len(constraints) == 0: return []

        msg = '[DeleteConstraint] Method not implemented. Constraints({:s}) are being ignored.'
        LOGGER.warning(msg.format(str(constraints)))
        return [True for _ in range(len(constraints))]

    def SetConfig(self, resources : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('resources', resources, list)
        if len(resources) == 0: return []

        results = []
        for resource in resources:
            try:
                resource_key, resource_value = resource
                resource_value = json.loads(resource_value)
                set_subnode_value(self.__resolver, self.__config, resource_key, resource_value)
                results.append(True)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to SetConfig({:s})'.format(str(resource)))
                results.append(e)

        return results

    def DeleteConfig(self, resources : List[Tuple[str, Any]]) -> List[Union[bool, Exception]]:
        chk_type('resources', resources, list)
        if len(resources) == 0: return []

        results = []
        for resource in resources:
            try:
                resource_key, _ = resource
                delete_subnode(self.__resolver, self.__config, resource_key)
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Unable to DeleteConfig({:s})'.format(str(resource)))
                results.append(e)

        return results
