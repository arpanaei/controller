package policy;

import io.grpc.BindableService;
import io.quarkus.grpc.GrpcService;
import io.quarkus.grpc.runtime.MutinyBean;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: policy.proto")
public class PolicyServiceBean extends MutinyPolicyServiceGrpc.PolicyServiceImplBase implements BindableService, MutinyBean {

    private final PolicyService delegate;

    PolicyServiceBean(@GrpcService PolicyService delegate) {
       this.delegate = delegate;
    }

    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyAdd(policy.Policy.PolicyRule request) {
       try {
         return delegate.policyAdd(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyUpdate(policy.Policy.PolicyRule request) {
       try {
         return delegate.policyUpdate(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyDelete(policy.Policy.PolicyRule request) {
       try {
         return delegate.policyDelete(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRule> getPolicy(policy.Policy.PolicyRuleId request) {
       try {
         return delegate.getPolicy(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByDeviceId(context.ContextOuterClass.DeviceId request) {
       try {
         return delegate.getPolicyByDeviceId(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }
    @Override
    public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByServiceId(context.ContextOuterClass.ServiceId request) {
       try {
         return delegate.getPolicyByServiceId(request);
       } catch (UnsupportedOperationException e) {
          throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
       }
    }

}