package policy;

import static policy.PolicyServiceGrpc.getServiceDescriptor;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: policy.proto")
public final class MutinyPolicyServiceGrpc implements io.quarkus.grpc.runtime.MutinyGrpc {
    private MutinyPolicyServiceGrpc() {}

    public static MutinyPolicyServiceStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyPolicyServiceStub(channel);
    }

    
    public static final class MutinyPolicyServiceStub extends io.grpc.stub.AbstractStub<MutinyPolicyServiceStub> implements io.quarkus.grpc.runtime.MutinyStub {
        private PolicyServiceGrpc.PolicyServiceStub delegateStub;

        private MutinyPolicyServiceStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = PolicyServiceGrpc.newStub(channel);
        }

        private MutinyPolicyServiceStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = PolicyServiceGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyPolicyServiceStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyPolicyServiceStub(channel, callOptions);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyAdd(policy.Policy.PolicyRule request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::policyAdd);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyUpdate(policy.Policy.PolicyRule request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::policyUpdate);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyDelete(policy.Policy.PolicyRule request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::policyDelete);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRule> getPolicy(policy.Policy.PolicyRuleId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getPolicy);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByDeviceId(context.ContextOuterClass.DeviceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getPolicyByDeviceId);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByServiceId(context.ContextOuterClass.ServiceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getPolicyByServiceId);
        }

    }

    
    public static abstract class PolicyServiceImplBase implements io.grpc.BindableService {

        private String compression;
        /**
        * Set whether the server will try to use a compressed response.
        *
        * @param compression the compression, e.g {@code gzip}
        */
        public PolicyServiceImplBase withCompression(String compression) {
        this.compression = compression;
        return this;
        }


        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyAdd(policy.Policy.PolicyRule request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyUpdate(policy.Policy.PolicyRule request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyDelete(policy.Policy.PolicyRule request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRule> getPolicy(policy.Policy.PolicyRuleId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByDeviceId(context.ContextOuterClass.DeviceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByServiceId(context.ContextOuterClass.ServiceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            policy.PolicyServiceGrpc.getPolicyAddMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            policy.Policy.PolicyRule,
                                            policy.Policy.PolicyRuleState>(
                                            this, METHODID_POLICY_ADD, compression)))
                    .addMethod(
                            policy.PolicyServiceGrpc.getPolicyUpdateMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            policy.Policy.PolicyRule,
                                            policy.Policy.PolicyRuleState>(
                                            this, METHODID_POLICY_UPDATE, compression)))
                    .addMethod(
                            policy.PolicyServiceGrpc.getPolicyDeleteMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            policy.Policy.PolicyRule,
                                            policy.Policy.PolicyRuleState>(
                                            this, METHODID_POLICY_DELETE, compression)))
                    .addMethod(
                            policy.PolicyServiceGrpc.getGetPolicyMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            policy.Policy.PolicyRuleId,
                                            policy.Policy.PolicyRule>(
                                            this, METHODID_GET_POLICY, compression)))
                    .addMethod(
                            policy.PolicyServiceGrpc.getGetPolicyByDeviceIdMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.DeviceId,
                                            policy.Policy.PolicyRuleList>(
                                            this, METHODID_GET_POLICY_BY_DEVICE_ID, compression)))
                    .addMethod(
                            policy.PolicyServiceGrpc.getGetPolicyByServiceIdMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ServiceId,
                                            policy.Policy.PolicyRuleList>(
                                            this, METHODID_GET_POLICY_BY_SERVICE_ID, compression)))
                    .build();
        }
    }

    private static final int METHODID_POLICY_ADD = 0;
    private static final int METHODID_POLICY_UPDATE = 1;
    private static final int METHODID_POLICY_DELETE = 2;
    private static final int METHODID_GET_POLICY = 3;
    private static final int METHODID_GET_POLICY_BY_DEVICE_ID = 4;
    private static final int METHODID_GET_POLICY_BY_SERVICE_ID = 5;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final PolicyServiceImplBase serviceImpl;
        private final int methodId;
        private final String compression;

        MethodHandlers(PolicyServiceImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_POLICY_ADD:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((policy.Policy.PolicyRule) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver,
                            compression,
                            serviceImpl::policyAdd);
                    break;
                case METHODID_POLICY_UPDATE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((policy.Policy.PolicyRule) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver,
                            compression,
                            serviceImpl::policyUpdate);
                    break;
                case METHODID_POLICY_DELETE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((policy.Policy.PolicyRule) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleState>) responseObserver,
                            compression,
                            serviceImpl::policyDelete);
                    break;
                case METHODID_GET_POLICY:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((policy.Policy.PolicyRuleId) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRule>) responseObserver,
                            compression,
                            serviceImpl::getPolicy);
                    break;
                case METHODID_GET_POLICY_BY_DEVICE_ID:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.DeviceId) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList>) responseObserver,
                            compression,
                            serviceImpl::getPolicyByDeviceId);
                    break;
                case METHODID_GET_POLICY_BY_SERVICE_ID:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ServiceId) request,
                            (io.grpc.stub.StreamObserver<policy.Policy.PolicyRuleList>) responseObserver,
                            compression,
                            serviceImpl::getPolicyByServiceId);
                    break;
                default:
                    throw new java.lang.AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new java.lang.AssertionError();
            }
        }
    }

}