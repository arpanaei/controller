package policy;

import io.quarkus.grpc.runtime.MutinyService;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: policy.proto")
public interface PolicyService extends MutinyService {

    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyAdd(policy.Policy.PolicyRule request);
    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyUpdate(policy.Policy.PolicyRule request);
    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleState> policyDelete(policy.Policy.PolicyRule request);
    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRule> getPolicy(policy.Policy.PolicyRuleId request);
    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByDeviceId(context.ContextOuterClass.DeviceId request);
    
    io.smallrye.mutiny.Uni<policy.Policy.PolicyRuleList> getPolicyByServiceId(context.ContextOuterClass.ServiceId request);
    
    
    

}