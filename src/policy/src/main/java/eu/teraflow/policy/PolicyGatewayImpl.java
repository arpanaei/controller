/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.policy;

import context.ContextOuterClass;
import io.quarkus.grpc.GrpcService;
import io.smallrye.mutiny.Uni;
import javax.inject.Inject;
import policy.Policy;

@GrpcService
public class PolicyGatewayImpl implements PolicyGateway {

    private final PolicyService policyService;

    @Inject
    public PolicyGatewayImpl(PolicyService policyService) {
        this.policyService = policyService;
    }

    @Override
    public Uni<Policy.PolicyRuleState> policyAdd(Policy.PolicyRule request) {
        return Uni.createFrom()
                .item(
                        () ->
                                Policy.PolicyRuleState.newBuilder()
                                        .setPolicyRuleId(request.getPolicyRuleId().getUuid())
                                        .build());
    }

    @Override
    public Uni<Policy.PolicyRuleState> policyUpdate(Policy.PolicyRule request) {
        return Uni.createFrom()
                .item(
                        () ->
                                Policy.PolicyRuleState.newBuilder()
                                        .setPolicyRuleId(request.getPolicyRuleId().getUuid())
                                        .build());
    }

    @Override
    public Uni<Policy.PolicyRuleState> policyDelete(Policy.PolicyRule request) {
        return Uni.createFrom()
                .item(
                        () ->
                                Policy.PolicyRuleState.newBuilder()
                                        .setPolicyRuleId(request.getPolicyRuleId().getUuid())
                                        .build());
    }

    @Override
    public Uni<Policy.PolicyRule> getPolicy(Policy.PolicyRuleId request) {
        return Uni.createFrom()
                .item(() -> Policy.PolicyRule.newBuilder().setPolicyRuleId(request).build());
    }

    @Override
    public Uni<Policy.PolicyRuleList> getPolicyByDeviceId(ContextOuterClass.DeviceId request) {

        return Uni.createFrom().item(() -> Policy.PolicyRuleList.newBuilder().build());
    }

    @Override
    public Uni<Policy.PolicyRuleList> getPolicyByServiceId(ContextOuterClass.ServiceId request) {

        return Uni.createFrom().item(() -> Policy.PolicyRuleList.newBuilder().build());
    }
}
