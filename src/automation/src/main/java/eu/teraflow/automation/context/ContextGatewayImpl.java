/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation.context;

import context.ContextOuterClass;
import context.MutinyContextServiceGrpc.MutinyContextServiceStub;
import eu.teraflow.automation.Serializer;
import eu.teraflow.automation.device.model.*;
import eu.teraflow.automation.device.model.Device;
import io.quarkus.grpc.GrpcClient;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;

@ApplicationScoped
public class ContextGatewayImpl implements ContextGateway {
    private static final Logger LOGGER = Logger.getLogger(ContextGatewayImpl.class);

    @GrpcClient("context")
    MutinyContextServiceStub streamingDelegateContext;

    private final Serializer serializer;

    @Inject
    public ContextGatewayImpl(Serializer serializer) {
        this.serializer = serializer;
    }

    @Override
    public Multi<DeviceEvent> getDeviceEvents() {
        final var serializedEmpty = ContextOuterClass.Empty.newBuilder().build();

        return streamingDelegateContext
                .getDeviceEvents(serializedEmpty)
                .onItem()
                .transform(serializer::deserialize);
    }

    @Override
    public Uni<Device> getDevice(String deviceId) {
        final var serializedDeviceId = serializer.serializeDeviceId(deviceId);

        return streamingDelegateContext
                .getDevice(serializedDeviceId)
                .onItem()
                .transform(serializer::deserialize);
    }
}
