/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation;

import eu.teraflow.automation.context.ContextService;
import eu.teraflow.automation.device.DeviceService;
import eu.teraflow.automation.device.model.Device;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;

@ApplicationScoped
public class AutomationServiceImpl implements AutomationService {
    private static final Logger LOGGER = Logger.getLogger(AutomationServiceImpl.class);

    private final DeviceService deviceService;
    private final ContextService contextService;

    @Inject
    public AutomationServiceImpl(DeviceService deviceService, ContextService contextService) {
        this.deviceService = deviceService;
        this.contextService = contextService;
    }

    @Override
    public Uni<Device> addDevice(String deviceId) {

        final var deserializedDeviceUni = contextService.getDevice(deviceId);

        deserializedDeviceUni
                // TODO fix subscribe
                .subscribe()
                .with(
                        device -> {
                            final var id = deviceId;

                            if (!device.isEnabled()) {
                                LOGGER.infof("Retrieved %s", device);

                                final var initialConfiguration =
                                        deviceService.getInitialConfiguration(device.getDeviceId());

                                device.enableDevice();
                                LOGGER.infof("Enabled device [%s]", id);

                                initialConfiguration
                                        .subscribe()
                                        .with(
                                                deviceConfig -> {
                                                    device.setDeviceConfiguration(deviceConfig);
                                                    final var configuredDeviceIdUni = deviceService.configureDevice(device);

                                                    configuredDeviceIdUni
                                                            .subscribe()
                                                            .with(
                                                                    configuredDeviceId ->
                                                                            LOGGER.infof(
                                                                                    "Device [%s] has been enabled and configured successfully with %s.\n",
                                                                                    id, deviceConfig));
                                                });
                            } else {
                                LOGGER.infof("%s has been already enabled. Ignoring...", device);
                            }
                        });

        return deserializedDeviceUni;
    }
}
