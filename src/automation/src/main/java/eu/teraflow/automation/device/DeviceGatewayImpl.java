/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation.device;

import context.ContextOuterClass;
import device.DeviceService;
import eu.teraflow.automation.Serializer;
import eu.teraflow.automation.device.model.Device;
import eu.teraflow.automation.device.model.DeviceConfig;
import io.quarkus.grpc.GrpcClient;
import io.smallrye.mutiny.Uni;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.jboss.logging.Logger;

@ApplicationScoped
public class DeviceGatewayImpl implements DeviceGateway {
    private static final Logger LOGGER = Logger.getLogger(DeviceGatewayImpl.class);

    @GrpcClient("device")
    DeviceService deviceDelegate;

    private final Serializer serializer;

    @Inject
    public DeviceGatewayImpl(Serializer serializer) {
        this.serializer = serializer;
    }

    @Override
    public Uni<DeviceConfig> getInitialConfiguration(String deviceId) {
        final var serializedDeviceId = serializer.serializeDeviceId(deviceId);

        return deviceDelegate
                .getInitialConfig(serializedDeviceId)
                .onItem()
                .transform(serializer::deserialize);
    }

    @Override
    public Uni<String> configureDevice(Device device) {
        final var serializedDevice = serializer.serialize(device);

        return deviceDelegate
                .configureDevice(serializedDevice)
                .onItem()
                .transform(serializer::deserialize);
    }
}
