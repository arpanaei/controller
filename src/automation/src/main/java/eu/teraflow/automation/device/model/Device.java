/*
 * Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.teraflow.automation.device.model;

public class Device {

    private final String deviceId;
    private final String deviceType;
    private DeviceConfig deviceConfig;
    private DeviceOperationalStatus deviceOperationalStatus;

    public Device(
            String deviceId,
            String deviceType,
            DeviceConfig deviceConfig,
            DeviceOperationalStatus deviceOperationalStatus) {

        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.deviceConfig = deviceConfig;
        this.deviceOperationalStatus = deviceOperationalStatus;
    }

    public Device(
            String deviceId, String deviceType, DeviceOperationalStatus deviceOperationalStatus) {
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.deviceOperationalStatus = deviceOperationalStatus;
    }

    public boolean isEnabled() {
        return deviceOperationalStatus == DeviceOperationalStatus.ENABLED;
    }

    public void enableDevice() {
        this.deviceOperationalStatus = DeviceOperationalStatus.ENABLED;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public DeviceConfig getDeviceConfig() {
        return deviceConfig;
    }

    public DeviceOperationalStatus getDeviceOperationalStatus() {
        return deviceOperationalStatus;
    }

    public void setDeviceConfiguration(DeviceConfig deviceConfig) {
        this.deviceConfig = deviceConfig;
    }

    @Override
    public String toString() {
        return String.format(
                "%s{id=\"%s\", type=\"%s\", operationalStatus=\"%s\", config=%s",
                getClass().getSimpleName(), deviceId, deviceType, deviceOperationalStatus, deviceConfig);
    }
}
