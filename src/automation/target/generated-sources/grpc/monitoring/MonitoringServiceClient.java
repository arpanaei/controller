package monitoring;

import java.util.function.BiFunction;

import io.quarkus.grpc.runtime.MutinyClient;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: monitoring.proto")
public class MonitoringServiceClient implements MonitoringService, MutinyClient<MutinyMonitoringServiceGrpc.MutinyMonitoringServiceStub> {

    private final MutinyMonitoringServiceGrpc.MutinyMonitoringServiceStub stub;

    public MonitoringServiceClient(String name, io.grpc.Channel channel, BiFunction<String, MutinyMonitoringServiceGrpc.MutinyMonitoringServiceStub, MutinyMonitoringServiceGrpc.MutinyMonitoringServiceStub> stubConfigurator) {
       this.stub = stubConfigurator.apply(name,MutinyMonitoringServiceGrpc.newMutinyStub(channel));
    }

    @Override
    public MutinyMonitoringServiceGrpc.MutinyMonitoringServiceStub getStub() {
       return stub;
    }

    @Override
    public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiId> createKpi(monitoring.Monitoring.KpiDescriptor request) {
       return stub.createKpi(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<monitoring.Monitoring.KpiDescriptor> getKpiDescriptor(monitoring.Monitoring.KpiId request) {
       return stub.getKpiDescriptor(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> includeKpi(monitoring.Monitoring.Kpi request) {
       return stub.includeKpi(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> monitorKpi(monitoring.Monitoring.MonitorKpiRequest request) {
       return stub.monitorKpi(request);
    }
    @Override
    public io.smallrye.mutiny.Uni<monitoring.Monitoring.Kpi> getInstantKpi(monitoring.Monitoring.KpiId request) {
       return stub.getInstantKpi(request);
    }

    @Override
    public io.smallrye.mutiny.Multi<monitoring.Monitoring.Kpi> getStreamKpi(monitoring.Monitoring.KpiId request) {
       return stub.getStreamKpi(request);
    }

}