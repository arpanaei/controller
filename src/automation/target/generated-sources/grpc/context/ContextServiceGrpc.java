package context;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.38.1)",
    comments = "Source: context.proto")
public final class ContextServiceGrpc {

  private ContextServiceGrpc() {}

  public static final String SERVICE_NAME = "context.ContextService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextIdList> getListContextIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListContextIds",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.ContextIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextIdList> getListContextIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextIdList> getListContextIdsMethod;
    if ((getListContextIdsMethod = ContextServiceGrpc.getListContextIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListContextIdsMethod = ContextServiceGrpc.getListContextIdsMethod) == null) {
          ContextServiceGrpc.getListContextIdsMethod = getListContextIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListContextIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListContextIds"))
              .build();
        }
      }
    }
    return getListContextIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextList> getListContextsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListContexts",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.ContextList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextList> getListContextsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextList> getListContextsMethod;
    if ((getListContextsMethod = ContextServiceGrpc.getListContextsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListContextsMethod = ContextServiceGrpc.getListContextsMethod) == null) {
          ContextServiceGrpc.getListContextsMethod = getListContextsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListContexts"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListContexts"))
              .build();
        }
      }
    }
    return getListContextsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.Context> getGetContextMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetContext",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.Context.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.Context> getGetContextMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.Context> getGetContextMethod;
    if ((getGetContextMethod = ContextServiceGrpc.getGetContextMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetContextMethod = ContextServiceGrpc.getGetContextMethod) == null) {
          ContextServiceGrpc.getGetContextMethod = getGetContextMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.Context>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetContext"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Context.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetContext"))
              .build();
        }
      }
    }
    return getGetContextMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Context,
      context.ContextOuterClass.ContextId> getSetContextMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetContext",
      requestType = context.ContextOuterClass.Context.class,
      responseType = context.ContextOuterClass.ContextId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Context,
      context.ContextOuterClass.ContextId> getSetContextMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Context, context.ContextOuterClass.ContextId> getSetContextMethod;
    if ((getSetContextMethod = ContextServiceGrpc.getSetContextMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetContextMethod = ContextServiceGrpc.getSetContextMethod) == null) {
          ContextServiceGrpc.getSetContextMethod = getSetContextMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Context, context.ContextOuterClass.ContextId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetContext"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Context.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetContext"))
              .build();
        }
      }
    }
    return getSetContextMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.Empty> getRemoveContextMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveContext",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.Empty> getRemoveContextMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.Empty> getRemoveContextMethod;
    if ((getRemoveContextMethod = ContextServiceGrpc.getRemoveContextMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveContextMethod = ContextServiceGrpc.getRemoveContextMethod) == null) {
          ContextServiceGrpc.getRemoveContextMethod = getRemoveContextMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveContext"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveContext"))
              .build();
        }
      }
    }
    return getRemoveContextMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextEvent> getGetContextEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetContextEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.ContextEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ContextEvent> getGetContextEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextEvent> getGetContextEventsMethod;
    if ((getGetContextEventsMethod = ContextServiceGrpc.getGetContextEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetContextEventsMethod = ContextServiceGrpc.getGetContextEventsMethod) == null) {
          ContextServiceGrpc.getGetContextEventsMethod = getGetContextEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.ContextEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetContextEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetContextEvents"))
              .build();
        }
      }
    }
    return getGetContextEventsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.TopologyIdList> getListTopologyIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListTopologyIds",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.TopologyIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.TopologyIdList> getListTopologyIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.TopologyIdList> getListTopologyIdsMethod;
    if ((getListTopologyIdsMethod = ContextServiceGrpc.getListTopologyIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListTopologyIdsMethod = ContextServiceGrpc.getListTopologyIdsMethod) == null) {
          ContextServiceGrpc.getListTopologyIdsMethod = getListTopologyIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.TopologyIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListTopologyIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListTopologyIds"))
              .build();
        }
      }
    }
    return getListTopologyIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.TopologyList> getListTopologiesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListTopologies",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.TopologyList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.TopologyList> getListTopologiesMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.TopologyList> getListTopologiesMethod;
    if ((getListTopologiesMethod = ContextServiceGrpc.getListTopologiesMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListTopologiesMethod = ContextServiceGrpc.getListTopologiesMethod) == null) {
          ContextServiceGrpc.getListTopologiesMethod = getListTopologiesMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.TopologyList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListTopologies"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListTopologies"))
              .build();
        }
      }
    }
    return getListTopologiesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId,
      context.ContextOuterClass.Topology> getGetTopologyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTopology",
      requestType = context.ContextOuterClass.TopologyId.class,
      responseType = context.ContextOuterClass.Topology.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId,
      context.ContextOuterClass.Topology> getGetTopologyMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId, context.ContextOuterClass.Topology> getGetTopologyMethod;
    if ((getGetTopologyMethod = ContextServiceGrpc.getGetTopologyMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetTopologyMethod = ContextServiceGrpc.getGetTopologyMethod) == null) {
          ContextServiceGrpc.getGetTopologyMethod = getGetTopologyMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.TopologyId, context.ContextOuterClass.Topology>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTopology"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Topology.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetTopology"))
              .build();
        }
      }
    }
    return getGetTopologyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Topology,
      context.ContextOuterClass.TopologyId> getSetTopologyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetTopology",
      requestType = context.ContextOuterClass.Topology.class,
      responseType = context.ContextOuterClass.TopologyId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Topology,
      context.ContextOuterClass.TopologyId> getSetTopologyMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Topology, context.ContextOuterClass.TopologyId> getSetTopologyMethod;
    if ((getSetTopologyMethod = ContextServiceGrpc.getSetTopologyMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetTopologyMethod = ContextServiceGrpc.getSetTopologyMethod) == null) {
          ContextServiceGrpc.getSetTopologyMethod = getSetTopologyMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Topology, context.ContextOuterClass.TopologyId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetTopology"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Topology.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetTopology"))
              .build();
        }
      }
    }
    return getSetTopologyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId,
      context.ContextOuterClass.Empty> getRemoveTopologyMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveTopology",
      requestType = context.ContextOuterClass.TopologyId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId,
      context.ContextOuterClass.Empty> getRemoveTopologyMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.TopologyId, context.ContextOuterClass.Empty> getRemoveTopologyMethod;
    if ((getRemoveTopologyMethod = ContextServiceGrpc.getRemoveTopologyMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveTopologyMethod = ContextServiceGrpc.getRemoveTopologyMethod) == null) {
          ContextServiceGrpc.getRemoveTopologyMethod = getRemoveTopologyMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.TopologyId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveTopology"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveTopology"))
              .build();
        }
      }
    }
    return getRemoveTopologyMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.TopologyEvent> getGetTopologyEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetTopologyEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.TopologyEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.TopologyEvent> getGetTopologyEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.TopologyEvent> getGetTopologyEventsMethod;
    if ((getGetTopologyEventsMethod = ContextServiceGrpc.getGetTopologyEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetTopologyEventsMethod = ContextServiceGrpc.getGetTopologyEventsMethod) == null) {
          ContextServiceGrpc.getGetTopologyEventsMethod = getGetTopologyEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.TopologyEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetTopologyEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.TopologyEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetTopologyEvents"))
              .build();
        }
      }
    }
    return getGetTopologyEventsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceIdList> getListDeviceIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListDeviceIds",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.DeviceIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceIdList> getListDeviceIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceIdList> getListDeviceIdsMethod;
    if ((getListDeviceIdsMethod = ContextServiceGrpc.getListDeviceIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListDeviceIdsMethod = ContextServiceGrpc.getListDeviceIdsMethod) == null) {
          ContextServiceGrpc.getListDeviceIdsMethod = getListDeviceIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListDeviceIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListDeviceIds"))
              .build();
        }
      }
    }
    return getListDeviceIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceList> getListDevicesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListDevices",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.DeviceList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceList> getListDevicesMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceList> getListDevicesMethod;
    if ((getListDevicesMethod = ContextServiceGrpc.getListDevicesMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListDevicesMethod = ContextServiceGrpc.getListDevicesMethod) == null) {
          ContextServiceGrpc.getListDevicesMethod = getListDevicesMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListDevices"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListDevices"))
              .build();
        }
      }
    }
    return getListDevicesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      context.ContextOuterClass.Device> getGetDeviceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetDevice",
      requestType = context.ContextOuterClass.DeviceId.class,
      responseType = context.ContextOuterClass.Device.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      context.ContextOuterClass.Device> getGetDeviceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId, context.ContextOuterClass.Device> getGetDeviceMethod;
    if ((getGetDeviceMethod = ContextServiceGrpc.getGetDeviceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetDeviceMethod = ContextServiceGrpc.getGetDeviceMethod) == null) {
          ContextServiceGrpc.getGetDeviceMethod = getGetDeviceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.DeviceId, context.ContextOuterClass.Device>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetDevice"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Device.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetDevice"))
              .build();
        }
      }
    }
    return getGetDeviceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Device,
      context.ContextOuterClass.DeviceId> getSetDeviceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetDevice",
      requestType = context.ContextOuterClass.Device.class,
      responseType = context.ContextOuterClass.DeviceId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Device,
      context.ContextOuterClass.DeviceId> getSetDeviceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Device, context.ContextOuterClass.DeviceId> getSetDeviceMethod;
    if ((getSetDeviceMethod = ContextServiceGrpc.getSetDeviceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetDeviceMethod = ContextServiceGrpc.getSetDeviceMethod) == null) {
          ContextServiceGrpc.getSetDeviceMethod = getSetDeviceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Device, context.ContextOuterClass.DeviceId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetDevice"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Device.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetDevice"))
              .build();
        }
      }
    }
    return getSetDeviceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      context.ContextOuterClass.Empty> getRemoveDeviceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveDevice",
      requestType = context.ContextOuterClass.DeviceId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId,
      context.ContextOuterClass.Empty> getRemoveDeviceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.DeviceId, context.ContextOuterClass.Empty> getRemoveDeviceMethod;
    if ((getRemoveDeviceMethod = ContextServiceGrpc.getRemoveDeviceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveDeviceMethod = ContextServiceGrpc.getRemoveDeviceMethod) == null) {
          ContextServiceGrpc.getRemoveDeviceMethod = getRemoveDeviceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.DeviceId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveDevice"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveDevice"))
              .build();
        }
      }
    }
    return getRemoveDeviceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceEvent> getGetDeviceEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetDeviceEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.DeviceEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.DeviceEvent> getGetDeviceEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceEvent> getGetDeviceEventsMethod;
    if ((getGetDeviceEventsMethod = ContextServiceGrpc.getGetDeviceEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetDeviceEventsMethod = ContextServiceGrpc.getGetDeviceEventsMethod) == null) {
          ContextServiceGrpc.getGetDeviceEventsMethod = getGetDeviceEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.DeviceEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetDeviceEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.DeviceEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetDeviceEvents"))
              .build();
        }
      }
    }
    return getGetDeviceEventsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkIdList> getListLinkIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListLinkIds",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.LinkIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkIdList> getListLinkIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkIdList> getListLinkIdsMethod;
    if ((getListLinkIdsMethod = ContextServiceGrpc.getListLinkIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListLinkIdsMethod = ContextServiceGrpc.getListLinkIdsMethod) == null) {
          ContextServiceGrpc.getListLinkIdsMethod = getListLinkIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListLinkIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListLinkIds"))
              .build();
        }
      }
    }
    return getListLinkIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkList> getListLinksMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListLinks",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.LinkList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkList> getListLinksMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkList> getListLinksMethod;
    if ((getListLinksMethod = ContextServiceGrpc.getListLinksMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListLinksMethod = ContextServiceGrpc.getListLinksMethod) == null) {
          ContextServiceGrpc.getListLinksMethod = getListLinksMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListLinks"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListLinks"))
              .build();
        }
      }
    }
    return getListLinksMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId,
      context.ContextOuterClass.Link> getGetLinkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetLink",
      requestType = context.ContextOuterClass.LinkId.class,
      responseType = context.ContextOuterClass.Link.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId,
      context.ContextOuterClass.Link> getGetLinkMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId, context.ContextOuterClass.Link> getGetLinkMethod;
    if ((getGetLinkMethod = ContextServiceGrpc.getGetLinkMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetLinkMethod = ContextServiceGrpc.getGetLinkMethod) == null) {
          ContextServiceGrpc.getGetLinkMethod = getGetLinkMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.LinkId, context.ContextOuterClass.Link>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetLink"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Link.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetLink"))
              .build();
        }
      }
    }
    return getGetLinkMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Link,
      context.ContextOuterClass.LinkId> getSetLinkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetLink",
      requestType = context.ContextOuterClass.Link.class,
      responseType = context.ContextOuterClass.LinkId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Link,
      context.ContextOuterClass.LinkId> getSetLinkMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Link, context.ContextOuterClass.LinkId> getSetLinkMethod;
    if ((getSetLinkMethod = ContextServiceGrpc.getSetLinkMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetLinkMethod = ContextServiceGrpc.getSetLinkMethod) == null) {
          ContextServiceGrpc.getSetLinkMethod = getSetLinkMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Link, context.ContextOuterClass.LinkId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetLink"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Link.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetLink"))
              .build();
        }
      }
    }
    return getSetLinkMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId,
      context.ContextOuterClass.Empty> getRemoveLinkMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveLink",
      requestType = context.ContextOuterClass.LinkId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId,
      context.ContextOuterClass.Empty> getRemoveLinkMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.LinkId, context.ContextOuterClass.Empty> getRemoveLinkMethod;
    if ((getRemoveLinkMethod = ContextServiceGrpc.getRemoveLinkMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveLinkMethod = ContextServiceGrpc.getRemoveLinkMethod) == null) {
          ContextServiceGrpc.getRemoveLinkMethod = getRemoveLinkMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.LinkId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveLink"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveLink"))
              .build();
        }
      }
    }
    return getRemoveLinkMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkEvent> getGetLinkEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetLinkEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.LinkEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.LinkEvent> getGetLinkEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkEvent> getGetLinkEventsMethod;
    if ((getGetLinkEventsMethod = ContextServiceGrpc.getGetLinkEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetLinkEventsMethod = ContextServiceGrpc.getGetLinkEventsMethod) == null) {
          ContextServiceGrpc.getGetLinkEventsMethod = getGetLinkEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.LinkEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetLinkEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.LinkEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetLinkEvents"))
              .build();
        }
      }
    }
    return getGetLinkEventsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.ServiceIdList> getListServiceIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListServiceIds",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.ServiceIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.ServiceIdList> getListServiceIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.ServiceIdList> getListServiceIdsMethod;
    if ((getListServiceIdsMethod = ContextServiceGrpc.getListServiceIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListServiceIdsMethod = ContextServiceGrpc.getListServiceIdsMethod) == null) {
          ContextServiceGrpc.getListServiceIdsMethod = getListServiceIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.ServiceIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListServiceIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListServiceIds"))
              .build();
        }
      }
    }
    return getListServiceIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.ServiceList> getListServicesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListServices",
      requestType = context.ContextOuterClass.ContextId.class,
      responseType = context.ContextOuterClass.ServiceList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId,
      context.ContextOuterClass.ServiceList> getListServicesMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ContextId, context.ContextOuterClass.ServiceList> getListServicesMethod;
    if ((getListServicesMethod = ContextServiceGrpc.getListServicesMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListServicesMethod = ContextServiceGrpc.getListServicesMethod) == null) {
          ContextServiceGrpc.getListServicesMethod = getListServicesMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ContextId, context.ContextOuterClass.ServiceList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListServices"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ContextId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListServices"))
              .build();
        }
      }
    }
    return getListServicesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.Service> getGetServiceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetService",
      requestType = context.ContextOuterClass.ServiceId.class,
      responseType = context.ContextOuterClass.Service.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.Service> getGetServiceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId, context.ContextOuterClass.Service> getGetServiceMethod;
    if ((getGetServiceMethod = ContextServiceGrpc.getGetServiceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetServiceMethod = ContextServiceGrpc.getGetServiceMethod) == null) {
          ContextServiceGrpc.getGetServiceMethod = getGetServiceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ServiceId, context.ContextOuterClass.Service>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetService"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Service.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetService"))
              .build();
        }
      }
    }
    return getGetServiceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Service,
      context.ContextOuterClass.ServiceId> getSetServiceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetService",
      requestType = context.ContextOuterClass.Service.class,
      responseType = context.ContextOuterClass.ServiceId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Service,
      context.ContextOuterClass.ServiceId> getSetServiceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Service, context.ContextOuterClass.ServiceId> getSetServiceMethod;
    if ((getSetServiceMethod = ContextServiceGrpc.getSetServiceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetServiceMethod = ContextServiceGrpc.getSetServiceMethod) == null) {
          ContextServiceGrpc.getSetServiceMethod = getSetServiceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Service, context.ContextOuterClass.ServiceId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetService"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Service.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetService"))
              .build();
        }
      }
    }
    return getSetServiceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.Empty> getRemoveServiceMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveService",
      requestType = context.ContextOuterClass.ServiceId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.Empty> getRemoveServiceMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId, context.ContextOuterClass.Empty> getRemoveServiceMethod;
    if ((getRemoveServiceMethod = ContextServiceGrpc.getRemoveServiceMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveServiceMethod = ContextServiceGrpc.getRemoveServiceMethod) == null) {
          ContextServiceGrpc.getRemoveServiceMethod = getRemoveServiceMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ServiceId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveService"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveService"))
              .build();
        }
      }
    }
    return getRemoveServiceMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ServiceEvent> getGetServiceEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetServiceEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.ServiceEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ServiceEvent> getGetServiceEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.ServiceEvent> getGetServiceEventsMethod;
    if ((getGetServiceEventsMethod = ContextServiceGrpc.getGetServiceEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetServiceEventsMethod = ContextServiceGrpc.getGetServiceEventsMethod) == null) {
          ContextServiceGrpc.getGetServiceEventsMethod = getGetServiceEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.ServiceEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetServiceEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetServiceEvents"))
              .build();
        }
      }
    }
    return getGetServiceEventsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.ConnectionIdList> getListConnectionIdsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListConnectionIds",
      requestType = context.ContextOuterClass.ServiceId.class,
      responseType = context.ContextOuterClass.ConnectionIdList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.ConnectionIdList> getListConnectionIdsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId, context.ContextOuterClass.ConnectionIdList> getListConnectionIdsMethod;
    if ((getListConnectionIdsMethod = ContextServiceGrpc.getListConnectionIdsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListConnectionIdsMethod = ContextServiceGrpc.getListConnectionIdsMethod) == null) {
          ContextServiceGrpc.getListConnectionIdsMethod = getListConnectionIdsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ServiceId, context.ContextOuterClass.ConnectionIdList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListConnectionIds"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionIdList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListConnectionIds"))
              .build();
        }
      }
    }
    return getListConnectionIdsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.ConnectionList> getListConnectionsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListConnections",
      requestType = context.ContextOuterClass.ServiceId.class,
      responseType = context.ContextOuterClass.ConnectionList.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId,
      context.ContextOuterClass.ConnectionList> getListConnectionsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ServiceId, context.ContextOuterClass.ConnectionList> getListConnectionsMethod;
    if ((getListConnectionsMethod = ContextServiceGrpc.getListConnectionsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getListConnectionsMethod = ContextServiceGrpc.getListConnectionsMethod) == null) {
          ContextServiceGrpc.getListConnectionsMethod = getListConnectionsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ServiceId, context.ContextOuterClass.ConnectionList>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListConnections"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ServiceId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionList.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("ListConnections"))
              .build();
        }
      }
    }
    return getListConnectionsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId,
      context.ContextOuterClass.Connection> getGetConnectionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetConnection",
      requestType = context.ContextOuterClass.ConnectionId.class,
      responseType = context.ContextOuterClass.Connection.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId,
      context.ContextOuterClass.Connection> getGetConnectionMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId, context.ContextOuterClass.Connection> getGetConnectionMethod;
    if ((getGetConnectionMethod = ContextServiceGrpc.getGetConnectionMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetConnectionMethod = ContextServiceGrpc.getGetConnectionMethod) == null) {
          ContextServiceGrpc.getGetConnectionMethod = getGetConnectionMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ConnectionId, context.ContextOuterClass.Connection>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetConnection"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Connection.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetConnection"))
              .build();
        }
      }
    }
    return getGetConnectionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Connection,
      context.ContextOuterClass.ConnectionId> getSetConnectionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SetConnection",
      requestType = context.ContextOuterClass.Connection.class,
      responseType = context.ContextOuterClass.ConnectionId.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Connection,
      context.ContextOuterClass.ConnectionId> getSetConnectionMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Connection, context.ContextOuterClass.ConnectionId> getSetConnectionMethod;
    if ((getSetConnectionMethod = ContextServiceGrpc.getSetConnectionMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getSetConnectionMethod = ContextServiceGrpc.getSetConnectionMethod) == null) {
          ContextServiceGrpc.getSetConnectionMethod = getSetConnectionMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Connection, context.ContextOuterClass.ConnectionId>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SetConnection"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Connection.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionId.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("SetConnection"))
              .build();
        }
      }
    }
    return getSetConnectionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId,
      context.ContextOuterClass.Empty> getRemoveConnectionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "RemoveConnection",
      requestType = context.ContextOuterClass.ConnectionId.class,
      responseType = context.ContextOuterClass.Empty.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId,
      context.ContextOuterClass.Empty> getRemoveConnectionMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.ConnectionId, context.ContextOuterClass.Empty> getRemoveConnectionMethod;
    if ((getRemoveConnectionMethod = ContextServiceGrpc.getRemoveConnectionMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getRemoveConnectionMethod = ContextServiceGrpc.getRemoveConnectionMethod) == null) {
          ContextServiceGrpc.getRemoveConnectionMethod = getRemoveConnectionMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.ConnectionId, context.ContextOuterClass.Empty>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "RemoveConnection"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionId.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("RemoveConnection"))
              .build();
        }
      }
    }
    return getRemoveConnectionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ConnectionEvent> getGetConnectionEventsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetConnectionEvents",
      requestType = context.ContextOuterClass.Empty.class,
      responseType = context.ContextOuterClass.ConnectionEvent.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<context.ContextOuterClass.Empty,
      context.ContextOuterClass.ConnectionEvent> getGetConnectionEventsMethod() {
    io.grpc.MethodDescriptor<context.ContextOuterClass.Empty, context.ContextOuterClass.ConnectionEvent> getGetConnectionEventsMethod;
    if ((getGetConnectionEventsMethod = ContextServiceGrpc.getGetConnectionEventsMethod) == null) {
      synchronized (ContextServiceGrpc.class) {
        if ((getGetConnectionEventsMethod = ContextServiceGrpc.getGetConnectionEventsMethod) == null) {
          ContextServiceGrpc.getGetConnectionEventsMethod = getGetConnectionEventsMethod =
              io.grpc.MethodDescriptor.<context.ContextOuterClass.Empty, context.ContextOuterClass.ConnectionEvent>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetConnectionEvents"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.Empty.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  context.ContextOuterClass.ConnectionEvent.getDefaultInstance()))
              .setSchemaDescriptor(new ContextServiceMethodDescriptorSupplier("GetConnectionEvents"))
              .build();
        }
      }
    }
    return getGetConnectionEventsMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ContextServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ContextServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ContextServiceStub>() {
        @java.lang.Override
        public ContextServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ContextServiceStub(channel, callOptions);
        }
      };
    return ContextServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ContextServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ContextServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ContextServiceBlockingStub>() {
        @java.lang.Override
        public ContextServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ContextServiceBlockingStub(channel, callOptions);
        }
      };
    return ContextServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ContextServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<ContextServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<ContextServiceFutureStub>() {
        @java.lang.Override
        public ContextServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new ContextServiceFutureStub(channel, callOptions);
        }
      };
    return ContextServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class ContextServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void listContextIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListContextIdsMethod(), responseObserver);
    }

    /**
     */
    public void listContexts(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListContextsMethod(), responseObserver);
    }

    /**
     */
    public void getContext(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Context> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetContextMethod(), responseObserver);
    }

    /**
     */
    public void setContext(context.ContextOuterClass.Context request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetContextMethod(), responseObserver);
    }

    /**
     */
    public void removeContext(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveContextMethod(), responseObserver);
    }

    /**
     */
    public void getContextEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetContextEventsMethod(), responseObserver);
    }

    /**
     */
    public void listTopologyIds(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListTopologyIdsMethod(), responseObserver);
    }

    /**
     */
    public void listTopologies(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListTopologiesMethod(), responseObserver);
    }

    /**
     */
    public void getTopology(context.ContextOuterClass.TopologyId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Topology> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetTopologyMethod(), responseObserver);
    }

    /**
     */
    public void setTopology(context.ContextOuterClass.Topology request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetTopologyMethod(), responseObserver);
    }

    /**
     */
    public void removeTopology(context.ContextOuterClass.TopologyId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveTopologyMethod(), responseObserver);
    }

    /**
     */
    public void getTopologyEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetTopologyEventsMethod(), responseObserver);
    }

    /**
     */
    public void listDeviceIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListDeviceIdsMethod(), responseObserver);
    }

    /**
     */
    public void listDevices(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListDevicesMethod(), responseObserver);
    }

    /**
     */
    public void getDevice(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Device> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetDeviceMethod(), responseObserver);
    }

    /**
     */
    public void setDevice(context.ContextOuterClass.Device request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetDeviceMethod(), responseObserver);
    }

    /**
     */
    public void removeDevice(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveDeviceMethod(), responseObserver);
    }

    /**
     */
    public void getDeviceEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetDeviceEventsMethod(), responseObserver);
    }

    /**
     */
    public void listLinkIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListLinkIdsMethod(), responseObserver);
    }

    /**
     */
    public void listLinks(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListLinksMethod(), responseObserver);
    }

    /**
     */
    public void getLink(context.ContextOuterClass.LinkId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Link> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetLinkMethod(), responseObserver);
    }

    /**
     */
    public void setLink(context.ContextOuterClass.Link request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetLinkMethod(), responseObserver);
    }

    /**
     */
    public void removeLink(context.ContextOuterClass.LinkId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveLinkMethod(), responseObserver);
    }

    /**
     */
    public void getLinkEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetLinkEventsMethod(), responseObserver);
    }

    /**
     */
    public void listServiceIds(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListServiceIdsMethod(), responseObserver);
    }

    /**
     */
    public void listServices(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListServicesMethod(), responseObserver);
    }

    /**
     */
    public void getService(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Service> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetServiceMethod(), responseObserver);
    }

    /**
     */
    public void setService(context.ContextOuterClass.Service request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetServiceMethod(), responseObserver);
    }

    /**
     */
    public void removeService(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveServiceMethod(), responseObserver);
    }

    /**
     */
    public void getServiceEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetServiceEventsMethod(), responseObserver);
    }

    /**
     */
    public void listConnectionIds(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionIdList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListConnectionIdsMethod(), responseObserver);
    }

    /**
     */
    public void listConnections(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionList> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListConnectionsMethod(), responseObserver);
    }

    /**
     */
    public void getConnection(context.ContextOuterClass.ConnectionId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Connection> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetConnectionMethod(), responseObserver);
    }

    /**
     */
    public void setConnection(context.ContextOuterClass.Connection request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionId> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSetConnectionMethod(), responseObserver);
    }

    /**
     */
    public void removeConnection(context.ContextOuterClass.ConnectionId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getRemoveConnectionMethod(), responseObserver);
    }

    /**
     */
    public void getConnectionEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionEvent> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetConnectionEventsMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getListContextIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.ContextIdList>(
                  this, METHODID_LIST_CONTEXT_IDS)))
          .addMethod(
            getListContextsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.ContextList>(
                  this, METHODID_LIST_CONTEXTS)))
          .addMethod(
            getGetContextMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.Context>(
                  this, METHODID_GET_CONTEXT)))
          .addMethod(
            getSetContextMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Context,
                context.ContextOuterClass.ContextId>(
                  this, METHODID_SET_CONTEXT)))
          .addMethod(
            getRemoveContextMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_CONTEXT)))
          .addMethod(
            getGetContextEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.ContextEvent>(
                  this, METHODID_GET_CONTEXT_EVENTS)))
          .addMethod(
            getListTopologyIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.TopologyIdList>(
                  this, METHODID_LIST_TOPOLOGY_IDS)))
          .addMethod(
            getListTopologiesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.TopologyList>(
                  this, METHODID_LIST_TOPOLOGIES)))
          .addMethod(
            getGetTopologyMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.TopologyId,
                context.ContextOuterClass.Topology>(
                  this, METHODID_GET_TOPOLOGY)))
          .addMethod(
            getSetTopologyMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Topology,
                context.ContextOuterClass.TopologyId>(
                  this, METHODID_SET_TOPOLOGY)))
          .addMethod(
            getRemoveTopologyMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.TopologyId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_TOPOLOGY)))
          .addMethod(
            getGetTopologyEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.TopologyEvent>(
                  this, METHODID_GET_TOPOLOGY_EVENTS)))
          .addMethod(
            getListDeviceIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.DeviceIdList>(
                  this, METHODID_LIST_DEVICE_IDS)))
          .addMethod(
            getListDevicesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.DeviceList>(
                  this, METHODID_LIST_DEVICES)))
          .addMethod(
            getGetDeviceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.DeviceId,
                context.ContextOuterClass.Device>(
                  this, METHODID_GET_DEVICE)))
          .addMethod(
            getSetDeviceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Device,
                context.ContextOuterClass.DeviceId>(
                  this, METHODID_SET_DEVICE)))
          .addMethod(
            getRemoveDeviceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.DeviceId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_DEVICE)))
          .addMethod(
            getGetDeviceEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.DeviceEvent>(
                  this, METHODID_GET_DEVICE_EVENTS)))
          .addMethod(
            getListLinkIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.LinkIdList>(
                  this, METHODID_LIST_LINK_IDS)))
          .addMethod(
            getListLinksMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.LinkList>(
                  this, METHODID_LIST_LINKS)))
          .addMethod(
            getGetLinkMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.LinkId,
                context.ContextOuterClass.Link>(
                  this, METHODID_GET_LINK)))
          .addMethod(
            getSetLinkMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Link,
                context.ContextOuterClass.LinkId>(
                  this, METHODID_SET_LINK)))
          .addMethod(
            getRemoveLinkMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.LinkId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_LINK)))
          .addMethod(
            getGetLinkEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.LinkEvent>(
                  this, METHODID_GET_LINK_EVENTS)))
          .addMethod(
            getListServiceIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.ServiceIdList>(
                  this, METHODID_LIST_SERVICE_IDS)))
          .addMethod(
            getListServicesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ContextId,
                context.ContextOuterClass.ServiceList>(
                  this, METHODID_LIST_SERVICES)))
          .addMethod(
            getGetServiceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ServiceId,
                context.ContextOuterClass.Service>(
                  this, METHODID_GET_SERVICE)))
          .addMethod(
            getSetServiceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Service,
                context.ContextOuterClass.ServiceId>(
                  this, METHODID_SET_SERVICE)))
          .addMethod(
            getRemoveServiceMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ServiceId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_SERVICE)))
          .addMethod(
            getGetServiceEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.ServiceEvent>(
                  this, METHODID_GET_SERVICE_EVENTS)))
          .addMethod(
            getListConnectionIdsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ServiceId,
                context.ContextOuterClass.ConnectionIdList>(
                  this, METHODID_LIST_CONNECTION_IDS)))
          .addMethod(
            getListConnectionsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ServiceId,
                context.ContextOuterClass.ConnectionList>(
                  this, METHODID_LIST_CONNECTIONS)))
          .addMethod(
            getGetConnectionMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ConnectionId,
                context.ContextOuterClass.Connection>(
                  this, METHODID_GET_CONNECTION)))
          .addMethod(
            getSetConnectionMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.Connection,
                context.ContextOuterClass.ConnectionId>(
                  this, METHODID_SET_CONNECTION)))
          .addMethod(
            getRemoveConnectionMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                context.ContextOuterClass.ConnectionId,
                context.ContextOuterClass.Empty>(
                  this, METHODID_REMOVE_CONNECTION)))
          .addMethod(
            getGetConnectionEventsMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                context.ContextOuterClass.Empty,
                context.ContextOuterClass.ConnectionEvent>(
                  this, METHODID_GET_CONNECTION_EVENTS)))
          .build();
    }
  }

  /**
   */
  public static final class ContextServiceStub extends io.grpc.stub.AbstractAsyncStub<ContextServiceStub> {
    private ContextServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ContextServiceStub(channel, callOptions);
    }

    /**
     */
    public void listContextIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListContextIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listContexts(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListContextsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getContext(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Context> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetContextMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setContext(context.ContextOuterClass.Context request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetContextMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeContext(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveContextMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getContextEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetContextEventsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listTopologyIds(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListTopologyIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listTopologies(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListTopologiesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getTopology(context.ContextOuterClass.TopologyId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Topology> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetTopologyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setTopology(context.ContextOuterClass.Topology request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetTopologyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeTopology(context.ContextOuterClass.TopologyId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveTopologyMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getTopologyEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetTopologyEventsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listDeviceIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListDeviceIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listDevices(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListDevicesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getDevice(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Device> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetDeviceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setDevice(context.ContextOuterClass.Device request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetDeviceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeDevice(context.ContextOuterClass.DeviceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveDeviceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getDeviceEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetDeviceEventsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listLinkIds(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListLinkIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listLinks(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListLinksMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getLink(context.ContextOuterClass.LinkId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Link> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetLinkMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setLink(context.ContextOuterClass.Link request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetLinkMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeLink(context.ContextOuterClass.LinkId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveLinkMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getLinkEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetLinkEventsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listServiceIds(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListServiceIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listServices(context.ContextOuterClass.ContextId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListServicesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getService(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Service> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetServiceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setService(context.ContextOuterClass.Service request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetServiceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeService(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveServiceMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getServiceEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetServiceEventsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listConnectionIds(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionIdList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListConnectionIdsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listConnections(context.ContextOuterClass.ServiceId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionList> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListConnectionsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getConnection(context.ContextOuterClass.ConnectionId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Connection> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetConnectionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void setConnection(context.ContextOuterClass.Connection request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionId> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSetConnectionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void removeConnection(context.ContextOuterClass.ConnectionId request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getRemoveConnectionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getConnectionEvents(context.ContextOuterClass.Empty request,
        io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionEvent> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getGetConnectionEventsMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ContextServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<ContextServiceBlockingStub> {
    private ContextServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ContextServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public context.ContextOuterClass.ContextIdList listContextIds(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListContextIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ContextList listContexts(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListContextsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Context getContext(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetContextMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ContextId setContext(context.ContextOuterClass.Context request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetContextMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeContext(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveContextMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.ContextEvent> getContextEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetContextEventsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.TopologyIdList listTopologyIds(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListTopologyIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.TopologyList listTopologies(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListTopologiesMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Topology getTopology(context.ContextOuterClass.TopologyId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetTopologyMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.TopologyId setTopology(context.ContextOuterClass.Topology request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetTopologyMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeTopology(context.ContextOuterClass.TopologyId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveTopologyMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.TopologyEvent> getTopologyEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetTopologyEventsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.DeviceIdList listDeviceIds(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListDeviceIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.DeviceList listDevices(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListDevicesMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Device getDevice(context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetDeviceMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.DeviceId setDevice(context.ContextOuterClass.Device request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetDeviceMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeDevice(context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveDeviceMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.DeviceEvent> getDeviceEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetDeviceEventsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.LinkIdList listLinkIds(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListLinkIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.LinkList listLinks(context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListLinksMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Link getLink(context.ContextOuterClass.LinkId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetLinkMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.LinkId setLink(context.ContextOuterClass.Link request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetLinkMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeLink(context.ContextOuterClass.LinkId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveLinkMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.LinkEvent> getLinkEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetLinkEventsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ServiceIdList listServiceIds(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListServiceIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ServiceList listServices(context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListServicesMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Service getService(context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetServiceMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ServiceId setService(context.ContextOuterClass.Service request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetServiceMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeService(context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveServiceMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.ServiceEvent> getServiceEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetServiceEventsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ConnectionIdList listConnectionIds(context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListConnectionIdsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ConnectionList listConnections(context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListConnectionsMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Connection getConnection(context.ContextOuterClass.ConnectionId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetConnectionMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.ConnectionId setConnection(context.ContextOuterClass.Connection request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSetConnectionMethod(), getCallOptions(), request);
    }

    /**
     */
    public context.ContextOuterClass.Empty removeConnection(context.ContextOuterClass.ConnectionId request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getRemoveConnectionMethod(), getCallOptions(), request);
    }

    /**
     */
    public java.util.Iterator<context.ContextOuterClass.ConnectionEvent> getConnectionEvents(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getGetConnectionEventsMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ContextServiceFutureStub extends io.grpc.stub.AbstractFutureStub<ContextServiceFutureStub> {
    private ContextServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ContextServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new ContextServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ContextIdList> listContextIds(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListContextIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ContextList> listContexts(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListContextsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Context> getContext(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetContextMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ContextId> setContext(
        context.ContextOuterClass.Context request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetContextMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeContext(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveContextMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.TopologyIdList> listTopologyIds(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListTopologyIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.TopologyList> listTopologies(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListTopologiesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Topology> getTopology(
        context.ContextOuterClass.TopologyId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetTopologyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.TopologyId> setTopology(
        context.ContextOuterClass.Topology request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetTopologyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeTopology(
        context.ContextOuterClass.TopologyId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveTopologyMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.DeviceIdList> listDeviceIds(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListDeviceIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.DeviceList> listDevices(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListDevicesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Device> getDevice(
        context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetDeviceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.DeviceId> setDevice(
        context.ContextOuterClass.Device request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetDeviceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeDevice(
        context.ContextOuterClass.DeviceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveDeviceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.LinkIdList> listLinkIds(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListLinkIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.LinkList> listLinks(
        context.ContextOuterClass.Empty request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListLinksMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Link> getLink(
        context.ContextOuterClass.LinkId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetLinkMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.LinkId> setLink(
        context.ContextOuterClass.Link request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetLinkMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeLink(
        context.ContextOuterClass.LinkId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveLinkMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ServiceIdList> listServiceIds(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListServiceIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ServiceList> listServices(
        context.ContextOuterClass.ContextId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListServicesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Service> getService(
        context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetServiceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ServiceId> setService(
        context.ContextOuterClass.Service request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetServiceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeService(
        context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveServiceMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ConnectionIdList> listConnectionIds(
        context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListConnectionIdsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ConnectionList> listConnections(
        context.ContextOuterClass.ServiceId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListConnectionsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Connection> getConnection(
        context.ContextOuterClass.ConnectionId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetConnectionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.ConnectionId> setConnection(
        context.ContextOuterClass.Connection request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSetConnectionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<context.ContextOuterClass.Empty> removeConnection(
        context.ContextOuterClass.ConnectionId request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getRemoveConnectionMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_LIST_CONTEXT_IDS = 0;
  private static final int METHODID_LIST_CONTEXTS = 1;
  private static final int METHODID_GET_CONTEXT = 2;
  private static final int METHODID_SET_CONTEXT = 3;
  private static final int METHODID_REMOVE_CONTEXT = 4;
  private static final int METHODID_GET_CONTEXT_EVENTS = 5;
  private static final int METHODID_LIST_TOPOLOGY_IDS = 6;
  private static final int METHODID_LIST_TOPOLOGIES = 7;
  private static final int METHODID_GET_TOPOLOGY = 8;
  private static final int METHODID_SET_TOPOLOGY = 9;
  private static final int METHODID_REMOVE_TOPOLOGY = 10;
  private static final int METHODID_GET_TOPOLOGY_EVENTS = 11;
  private static final int METHODID_LIST_DEVICE_IDS = 12;
  private static final int METHODID_LIST_DEVICES = 13;
  private static final int METHODID_GET_DEVICE = 14;
  private static final int METHODID_SET_DEVICE = 15;
  private static final int METHODID_REMOVE_DEVICE = 16;
  private static final int METHODID_GET_DEVICE_EVENTS = 17;
  private static final int METHODID_LIST_LINK_IDS = 18;
  private static final int METHODID_LIST_LINKS = 19;
  private static final int METHODID_GET_LINK = 20;
  private static final int METHODID_SET_LINK = 21;
  private static final int METHODID_REMOVE_LINK = 22;
  private static final int METHODID_GET_LINK_EVENTS = 23;
  private static final int METHODID_LIST_SERVICE_IDS = 24;
  private static final int METHODID_LIST_SERVICES = 25;
  private static final int METHODID_GET_SERVICE = 26;
  private static final int METHODID_SET_SERVICE = 27;
  private static final int METHODID_REMOVE_SERVICE = 28;
  private static final int METHODID_GET_SERVICE_EVENTS = 29;
  private static final int METHODID_LIST_CONNECTION_IDS = 30;
  private static final int METHODID_LIST_CONNECTIONS = 31;
  private static final int METHODID_GET_CONNECTION = 32;
  private static final int METHODID_SET_CONNECTION = 33;
  private static final int METHODID_REMOVE_CONNECTION = 34;
  private static final int METHODID_GET_CONNECTION_EVENTS = 35;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ContextServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ContextServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_LIST_CONTEXT_IDS:
          serviceImpl.listContextIds((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextIdList>) responseObserver);
          break;
        case METHODID_LIST_CONTEXTS:
          serviceImpl.listContexts((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextList>) responseObserver);
          break;
        case METHODID_GET_CONTEXT:
          serviceImpl.getContext((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Context>) responseObserver);
          break;
        case METHODID_SET_CONTEXT:
          serviceImpl.setContext((context.ContextOuterClass.Context) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextId>) responseObserver);
          break;
        case METHODID_REMOVE_CONTEXT:
          serviceImpl.removeContext((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_CONTEXT_EVENTS:
          serviceImpl.getContextEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextEvent>) responseObserver);
          break;
        case METHODID_LIST_TOPOLOGY_IDS:
          serviceImpl.listTopologyIds((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyIdList>) responseObserver);
          break;
        case METHODID_LIST_TOPOLOGIES:
          serviceImpl.listTopologies((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyList>) responseObserver);
          break;
        case METHODID_GET_TOPOLOGY:
          serviceImpl.getTopology((context.ContextOuterClass.TopologyId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Topology>) responseObserver);
          break;
        case METHODID_SET_TOPOLOGY:
          serviceImpl.setTopology((context.ContextOuterClass.Topology) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyId>) responseObserver);
          break;
        case METHODID_REMOVE_TOPOLOGY:
          serviceImpl.removeTopology((context.ContextOuterClass.TopologyId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_TOPOLOGY_EVENTS:
          serviceImpl.getTopologyEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyEvent>) responseObserver);
          break;
        case METHODID_LIST_DEVICE_IDS:
          serviceImpl.listDeviceIds((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceIdList>) responseObserver);
          break;
        case METHODID_LIST_DEVICES:
          serviceImpl.listDevices((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceList>) responseObserver);
          break;
        case METHODID_GET_DEVICE:
          serviceImpl.getDevice((context.ContextOuterClass.DeviceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Device>) responseObserver);
          break;
        case METHODID_SET_DEVICE:
          serviceImpl.setDevice((context.ContextOuterClass.Device) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceId>) responseObserver);
          break;
        case METHODID_REMOVE_DEVICE:
          serviceImpl.removeDevice((context.ContextOuterClass.DeviceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_DEVICE_EVENTS:
          serviceImpl.getDeviceEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceEvent>) responseObserver);
          break;
        case METHODID_LIST_LINK_IDS:
          serviceImpl.listLinkIds((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkIdList>) responseObserver);
          break;
        case METHODID_LIST_LINKS:
          serviceImpl.listLinks((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkList>) responseObserver);
          break;
        case METHODID_GET_LINK:
          serviceImpl.getLink((context.ContextOuterClass.LinkId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Link>) responseObserver);
          break;
        case METHODID_SET_LINK:
          serviceImpl.setLink((context.ContextOuterClass.Link) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkId>) responseObserver);
          break;
        case METHODID_REMOVE_LINK:
          serviceImpl.removeLink((context.ContextOuterClass.LinkId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_LINK_EVENTS:
          serviceImpl.getLinkEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkEvent>) responseObserver);
          break;
        case METHODID_LIST_SERVICE_IDS:
          serviceImpl.listServiceIds((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceIdList>) responseObserver);
          break;
        case METHODID_LIST_SERVICES:
          serviceImpl.listServices((context.ContextOuterClass.ContextId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceList>) responseObserver);
          break;
        case METHODID_GET_SERVICE:
          serviceImpl.getService((context.ContextOuterClass.ServiceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Service>) responseObserver);
          break;
        case METHODID_SET_SERVICE:
          serviceImpl.setService((context.ContextOuterClass.Service) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceId>) responseObserver);
          break;
        case METHODID_REMOVE_SERVICE:
          serviceImpl.removeService((context.ContextOuterClass.ServiceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_SERVICE_EVENTS:
          serviceImpl.getServiceEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceEvent>) responseObserver);
          break;
        case METHODID_LIST_CONNECTION_IDS:
          serviceImpl.listConnectionIds((context.ContextOuterClass.ServiceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionIdList>) responseObserver);
          break;
        case METHODID_LIST_CONNECTIONS:
          serviceImpl.listConnections((context.ContextOuterClass.ServiceId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionList>) responseObserver);
          break;
        case METHODID_GET_CONNECTION:
          serviceImpl.getConnection((context.ContextOuterClass.ConnectionId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Connection>) responseObserver);
          break;
        case METHODID_SET_CONNECTION:
          serviceImpl.setConnection((context.ContextOuterClass.Connection) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionId>) responseObserver);
          break;
        case METHODID_REMOVE_CONNECTION:
          serviceImpl.removeConnection((context.ContextOuterClass.ConnectionId) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver);
          break;
        case METHODID_GET_CONNECTION_EVENTS:
          serviceImpl.getConnectionEvents((context.ContextOuterClass.Empty) request,
              (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionEvent>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ContextServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ContextServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return context.ContextOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ContextService");
    }
  }

  private static final class ContextServiceFileDescriptorSupplier
      extends ContextServiceBaseDescriptorSupplier {
    ContextServiceFileDescriptorSupplier() {}
  }

  private static final class ContextServiceMethodDescriptorSupplier
      extends ContextServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ContextServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ContextServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ContextServiceFileDescriptorSupplier())
              .addMethod(getListContextIdsMethod())
              .addMethod(getListContextsMethod())
              .addMethod(getGetContextMethod())
              .addMethod(getSetContextMethod())
              .addMethod(getRemoveContextMethod())
              .addMethod(getGetContextEventsMethod())
              .addMethod(getListTopologyIdsMethod())
              .addMethod(getListTopologiesMethod())
              .addMethod(getGetTopologyMethod())
              .addMethod(getSetTopologyMethod())
              .addMethod(getRemoveTopologyMethod())
              .addMethod(getGetTopologyEventsMethod())
              .addMethod(getListDeviceIdsMethod())
              .addMethod(getListDevicesMethod())
              .addMethod(getGetDeviceMethod())
              .addMethod(getSetDeviceMethod())
              .addMethod(getRemoveDeviceMethod())
              .addMethod(getGetDeviceEventsMethod())
              .addMethod(getListLinkIdsMethod())
              .addMethod(getListLinksMethod())
              .addMethod(getGetLinkMethod())
              .addMethod(getSetLinkMethod())
              .addMethod(getRemoveLinkMethod())
              .addMethod(getGetLinkEventsMethod())
              .addMethod(getListServiceIdsMethod())
              .addMethod(getListServicesMethod())
              .addMethod(getGetServiceMethod())
              .addMethod(getSetServiceMethod())
              .addMethod(getRemoveServiceMethod())
              .addMethod(getGetServiceEventsMethod())
              .addMethod(getListConnectionIdsMethod())
              .addMethod(getListConnectionsMethod())
              .addMethod(getGetConnectionMethod())
              .addMethod(getSetConnectionMethod())
              .addMethod(getRemoveConnectionMethod())
              .addMethod(getGetConnectionEventsMethod())
              .build();
        }
      }
    }
    return result;
  }
}
