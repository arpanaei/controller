package context;

import static context.ContextServiceGrpc.getServiceDescriptor;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;

@javax.annotation.Generated(
value = "by Mutiny Grpc generator",
comments = "Source: context.proto")
public final class MutinyContextServiceGrpc implements io.quarkus.grpc.runtime.MutinyGrpc {
    private MutinyContextServiceGrpc() {}

    public static MutinyContextServiceStub newMutinyStub(io.grpc.Channel channel) {
        return new MutinyContextServiceStub(channel);
    }

    
    public static final class MutinyContextServiceStub extends io.grpc.stub.AbstractStub<MutinyContextServiceStub> implements io.quarkus.grpc.runtime.MutinyStub {
        private ContextServiceGrpc.ContextServiceStub delegateStub;

        private MutinyContextServiceStub(io.grpc.Channel channel) {
            super(channel);
            delegateStub = ContextServiceGrpc.newStub(channel);
        }

        private MutinyContextServiceStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            super(channel, callOptions);
            delegateStub = ContextServiceGrpc.newStub(channel).build(channel, callOptions);
        }

        @Override
        protected MutinyContextServiceStub build(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
            return new MutinyContextServiceStub(channel, callOptions);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextIdList> listContextIds(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listContextIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextList> listContexts(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listContexts);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Context> getContext(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getContext);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextId> setContext(context.ContextOuterClass.Context request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setContext);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeContext(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeContext);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyIdList> listTopologyIds(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listTopologyIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyList> listTopologies(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listTopologies);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Topology> getTopology(context.ContextOuterClass.TopologyId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getTopology);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyId> setTopology(context.ContextOuterClass.Topology request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setTopology);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeTopology(context.ContextOuterClass.TopologyId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeTopology);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceIdList> listDeviceIds(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listDeviceIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceList> listDevices(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listDevices);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Device> getDevice(context.ContextOuterClass.DeviceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getDevice);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceId> setDevice(context.ContextOuterClass.Device request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setDevice);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeDevice(context.ContextOuterClass.DeviceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeDevice);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkIdList> listLinkIds(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listLinkIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkList> listLinks(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listLinks);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Link> getLink(context.ContextOuterClass.LinkId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getLink);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkId> setLink(context.ContextOuterClass.Link request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setLink);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeLink(context.ContextOuterClass.LinkId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeLink);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceIdList> listServiceIds(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listServiceIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceList> listServices(context.ContextOuterClass.ContextId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listServices);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Service> getService(context.ContextOuterClass.ServiceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getService);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceId> setService(context.ContextOuterClass.Service request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setService);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeService(context.ContextOuterClass.ServiceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeService);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionIdList> listConnectionIds(context.ContextOuterClass.ServiceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listConnectionIds);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionList> listConnections(context.ContextOuterClass.ServiceId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::listConnections);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Connection> getConnection(context.ContextOuterClass.ConnectionId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::getConnection);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionId> setConnection(context.ContextOuterClass.Connection request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::setConnection);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeConnection(context.ContextOuterClass.ConnectionId request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToOne(request, delegateStub::removeConnection);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ContextEvent> getContextEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getContextEvents);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.TopologyEvent> getTopologyEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getTopologyEvents);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.DeviceEvent> getDeviceEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getDeviceEvents);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.LinkEvent> getLinkEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getLinkEvents);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ServiceEvent> getServiceEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getServiceEvents);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ConnectionEvent> getConnectionEvents(context.ContextOuterClass.Empty request) {
            return io.quarkus.grpc.runtime.ClientCalls.oneToMany(request, delegateStub::getConnectionEvents);
        }

    }

    
    public static abstract class ContextServiceImplBase implements io.grpc.BindableService {

        private String compression;
        /**
        * Set whether the server will try to use a compressed response.
        *
        * @param compression the compression, e.g {@code gzip}
        */
        public ContextServiceImplBase withCompression(String compression) {
        this.compression = compression;
        return this;
        }


        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextIdList> listContextIds(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextList> listContexts(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Context> getContext(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ContextId> setContext(context.ContextOuterClass.Context request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeContext(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyIdList> listTopologyIds(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyList> listTopologies(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Topology> getTopology(context.ContextOuterClass.TopologyId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.TopologyId> setTopology(context.ContextOuterClass.Topology request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeTopology(context.ContextOuterClass.TopologyId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceIdList> listDeviceIds(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceList> listDevices(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Device> getDevice(context.ContextOuterClass.DeviceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.DeviceId> setDevice(context.ContextOuterClass.Device request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeDevice(context.ContextOuterClass.DeviceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkIdList> listLinkIds(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkList> listLinks(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Link> getLink(context.ContextOuterClass.LinkId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.LinkId> setLink(context.ContextOuterClass.Link request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeLink(context.ContextOuterClass.LinkId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceIdList> listServiceIds(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceList> listServices(context.ContextOuterClass.ContextId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Service> getService(context.ContextOuterClass.ServiceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ServiceId> setService(context.ContextOuterClass.Service request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeService(context.ContextOuterClass.ServiceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionIdList> listConnectionIds(context.ContextOuterClass.ServiceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionList> listConnections(context.ContextOuterClass.ServiceId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Connection> getConnection(context.ContextOuterClass.ConnectionId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.ConnectionId> setConnection(context.ContextOuterClass.Connection request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Uni<context.ContextOuterClass.Empty> removeConnection(context.ContextOuterClass.ConnectionId request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ContextEvent> getContextEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.TopologyEvent> getTopologyEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.DeviceEvent> getDeviceEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.LinkEvent> getLinkEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ServiceEvent> getServiceEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        
        public io.smallrye.mutiny.Multi<context.ContextOuterClass.ConnectionEvent> getConnectionEvents(context.ContextOuterClass.Empty request) {
            throw new io.grpc.StatusRuntimeException(io.grpc.Status.UNIMPLEMENTED);
        }

        @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
            return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
                    .addMethod(
                            context.ContextServiceGrpc.getListContextIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.ContextIdList>(
                                            this, METHODID_LIST_CONTEXT_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListContextsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.ContextList>(
                                            this, METHODID_LIST_CONTEXTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetContextMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.Context>(
                                            this, METHODID_GET_CONTEXT, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetContextMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Context,
                                            context.ContextOuterClass.ContextId>(
                                            this, METHODID_SET_CONTEXT, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveContextMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_CONTEXT, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetContextEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.ContextEvent>(
                                            this, METHODID_GET_CONTEXT_EVENTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListTopologyIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.TopologyIdList>(
                                            this, METHODID_LIST_TOPOLOGY_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListTopologiesMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.TopologyList>(
                                            this, METHODID_LIST_TOPOLOGIES, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetTopologyMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.TopologyId,
                                            context.ContextOuterClass.Topology>(
                                            this, METHODID_GET_TOPOLOGY, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetTopologyMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Topology,
                                            context.ContextOuterClass.TopologyId>(
                                            this, METHODID_SET_TOPOLOGY, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveTopologyMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.TopologyId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_TOPOLOGY, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetTopologyEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.TopologyEvent>(
                                            this, METHODID_GET_TOPOLOGY_EVENTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListDeviceIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.DeviceIdList>(
                                            this, METHODID_LIST_DEVICE_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListDevicesMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.DeviceList>(
                                            this, METHODID_LIST_DEVICES, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetDeviceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.DeviceId,
                                            context.ContextOuterClass.Device>(
                                            this, METHODID_GET_DEVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetDeviceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Device,
                                            context.ContextOuterClass.DeviceId>(
                                            this, METHODID_SET_DEVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveDeviceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.DeviceId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_DEVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetDeviceEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.DeviceEvent>(
                                            this, METHODID_GET_DEVICE_EVENTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListLinkIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.LinkIdList>(
                                            this, METHODID_LIST_LINK_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListLinksMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.LinkList>(
                                            this, METHODID_LIST_LINKS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetLinkMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.LinkId,
                                            context.ContextOuterClass.Link>(
                                            this, METHODID_GET_LINK, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetLinkMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Link,
                                            context.ContextOuterClass.LinkId>(
                                            this, METHODID_SET_LINK, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveLinkMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.LinkId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_LINK, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetLinkEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.LinkEvent>(
                                            this, METHODID_GET_LINK_EVENTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListServiceIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.ServiceIdList>(
                                            this, METHODID_LIST_SERVICE_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListServicesMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ContextId,
                                            context.ContextOuterClass.ServiceList>(
                                            this, METHODID_LIST_SERVICES, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetServiceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ServiceId,
                                            context.ContextOuterClass.Service>(
                                            this, METHODID_GET_SERVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetServiceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Service,
                                            context.ContextOuterClass.ServiceId>(
                                            this, METHODID_SET_SERVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveServiceMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ServiceId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_SERVICE, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetServiceEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.ServiceEvent>(
                                            this, METHODID_GET_SERVICE_EVENTS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListConnectionIdsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ServiceId,
                                            context.ContextOuterClass.ConnectionIdList>(
                                            this, METHODID_LIST_CONNECTION_IDS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getListConnectionsMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ServiceId,
                                            context.ContextOuterClass.ConnectionList>(
                                            this, METHODID_LIST_CONNECTIONS, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetConnectionMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ConnectionId,
                                            context.ContextOuterClass.Connection>(
                                            this, METHODID_GET_CONNECTION, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getSetConnectionMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Connection,
                                            context.ContextOuterClass.ConnectionId>(
                                            this, METHODID_SET_CONNECTION, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getRemoveConnectionMethod(),
                            asyncUnaryCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.ConnectionId,
                                            context.ContextOuterClass.Empty>(
                                            this, METHODID_REMOVE_CONNECTION, compression)))
                    .addMethod(
                            context.ContextServiceGrpc.getGetConnectionEventsMethod(),
                            asyncServerStreamingCall(
                                    new MethodHandlers<
                                            context.ContextOuterClass.Empty,
                                            context.ContextOuterClass.ConnectionEvent>(
                                            this, METHODID_GET_CONNECTION_EVENTS, compression)))
                    .build();
        }
    }

    private static final int METHODID_LIST_CONTEXT_IDS = 0;
    private static final int METHODID_LIST_CONTEXTS = 1;
    private static final int METHODID_GET_CONTEXT = 2;
    private static final int METHODID_SET_CONTEXT = 3;
    private static final int METHODID_REMOVE_CONTEXT = 4;
    private static final int METHODID_GET_CONTEXT_EVENTS = 5;
    private static final int METHODID_LIST_TOPOLOGY_IDS = 6;
    private static final int METHODID_LIST_TOPOLOGIES = 7;
    private static final int METHODID_GET_TOPOLOGY = 8;
    private static final int METHODID_SET_TOPOLOGY = 9;
    private static final int METHODID_REMOVE_TOPOLOGY = 10;
    private static final int METHODID_GET_TOPOLOGY_EVENTS = 11;
    private static final int METHODID_LIST_DEVICE_IDS = 12;
    private static final int METHODID_LIST_DEVICES = 13;
    private static final int METHODID_GET_DEVICE = 14;
    private static final int METHODID_SET_DEVICE = 15;
    private static final int METHODID_REMOVE_DEVICE = 16;
    private static final int METHODID_GET_DEVICE_EVENTS = 17;
    private static final int METHODID_LIST_LINK_IDS = 18;
    private static final int METHODID_LIST_LINKS = 19;
    private static final int METHODID_GET_LINK = 20;
    private static final int METHODID_SET_LINK = 21;
    private static final int METHODID_REMOVE_LINK = 22;
    private static final int METHODID_GET_LINK_EVENTS = 23;
    private static final int METHODID_LIST_SERVICE_IDS = 24;
    private static final int METHODID_LIST_SERVICES = 25;
    private static final int METHODID_GET_SERVICE = 26;
    private static final int METHODID_SET_SERVICE = 27;
    private static final int METHODID_REMOVE_SERVICE = 28;
    private static final int METHODID_GET_SERVICE_EVENTS = 29;
    private static final int METHODID_LIST_CONNECTION_IDS = 30;
    private static final int METHODID_LIST_CONNECTIONS = 31;
    private static final int METHODID_GET_CONNECTION = 32;
    private static final int METHODID_SET_CONNECTION = 33;
    private static final int METHODID_REMOVE_CONNECTION = 34;
    private static final int METHODID_GET_CONNECTION_EVENTS = 35;

    private static final class MethodHandlers<Req, Resp> implements
            io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
            io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
        private final ContextServiceImplBase serviceImpl;
        private final int methodId;
        private final String compression;

        MethodHandlers(ContextServiceImplBase serviceImpl, int methodId, String compression) {
            this.serviceImpl = serviceImpl;
            this.methodId = methodId;
            this.compression = compression;
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                case METHODID_LIST_CONTEXT_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextIdList>) responseObserver,
                            compression,
                            serviceImpl::listContextIds);
                    break;
                case METHODID_LIST_CONTEXTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextList>) responseObserver,
                            compression,
                            serviceImpl::listContexts);
                    break;
                case METHODID_GET_CONTEXT:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Context>) responseObserver,
                            compression,
                            serviceImpl::getContext);
                    break;
                case METHODID_SET_CONTEXT:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Context) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextId>) responseObserver,
                            compression,
                            serviceImpl::setContext);
                    break;
                case METHODID_REMOVE_CONTEXT:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeContext);
                    break;
                case METHODID_GET_CONTEXT_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ContextEvent>) responseObserver,
                            compression,
                            serviceImpl::getContextEvents);
                    break;
                case METHODID_LIST_TOPOLOGY_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyIdList>) responseObserver,
                            compression,
                            serviceImpl::listTopologyIds);
                    break;
                case METHODID_LIST_TOPOLOGIES:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyList>) responseObserver,
                            compression,
                            serviceImpl::listTopologies);
                    break;
                case METHODID_GET_TOPOLOGY:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.TopologyId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Topology>) responseObserver,
                            compression,
                            serviceImpl::getTopology);
                    break;
                case METHODID_SET_TOPOLOGY:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Topology) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyId>) responseObserver,
                            compression,
                            serviceImpl::setTopology);
                    break;
                case METHODID_REMOVE_TOPOLOGY:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.TopologyId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeTopology);
                    break;
                case METHODID_GET_TOPOLOGY_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.TopologyEvent>) responseObserver,
                            compression,
                            serviceImpl::getTopologyEvents);
                    break;
                case METHODID_LIST_DEVICE_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceIdList>) responseObserver,
                            compression,
                            serviceImpl::listDeviceIds);
                    break;
                case METHODID_LIST_DEVICES:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceList>) responseObserver,
                            compression,
                            serviceImpl::listDevices);
                    break;
                case METHODID_GET_DEVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.DeviceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Device>) responseObserver,
                            compression,
                            serviceImpl::getDevice);
                    break;
                case METHODID_SET_DEVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Device) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceId>) responseObserver,
                            compression,
                            serviceImpl::setDevice);
                    break;
                case METHODID_REMOVE_DEVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.DeviceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeDevice);
                    break;
                case METHODID_GET_DEVICE_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.DeviceEvent>) responseObserver,
                            compression,
                            serviceImpl::getDeviceEvents);
                    break;
                case METHODID_LIST_LINK_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkIdList>) responseObserver,
                            compression,
                            serviceImpl::listLinkIds);
                    break;
                case METHODID_LIST_LINKS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkList>) responseObserver,
                            compression,
                            serviceImpl::listLinks);
                    break;
                case METHODID_GET_LINK:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.LinkId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Link>) responseObserver,
                            compression,
                            serviceImpl::getLink);
                    break;
                case METHODID_SET_LINK:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Link) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkId>) responseObserver,
                            compression,
                            serviceImpl::setLink);
                    break;
                case METHODID_REMOVE_LINK:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.LinkId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeLink);
                    break;
                case METHODID_GET_LINK_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.LinkEvent>) responseObserver,
                            compression,
                            serviceImpl::getLinkEvents);
                    break;
                case METHODID_LIST_SERVICE_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceIdList>) responseObserver,
                            compression,
                            serviceImpl::listServiceIds);
                    break;
                case METHODID_LIST_SERVICES:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ContextId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceList>) responseObserver,
                            compression,
                            serviceImpl::listServices);
                    break;
                case METHODID_GET_SERVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ServiceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Service>) responseObserver,
                            compression,
                            serviceImpl::getService);
                    break;
                case METHODID_SET_SERVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Service) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceId>) responseObserver,
                            compression,
                            serviceImpl::setService);
                    break;
                case METHODID_REMOVE_SERVICE:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ServiceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeService);
                    break;
                case METHODID_GET_SERVICE_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ServiceEvent>) responseObserver,
                            compression,
                            serviceImpl::getServiceEvents);
                    break;
                case METHODID_LIST_CONNECTION_IDS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ServiceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionIdList>) responseObserver,
                            compression,
                            serviceImpl::listConnectionIds);
                    break;
                case METHODID_LIST_CONNECTIONS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ServiceId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionList>) responseObserver,
                            compression,
                            serviceImpl::listConnections);
                    break;
                case METHODID_GET_CONNECTION:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ConnectionId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Connection>) responseObserver,
                            compression,
                            serviceImpl::getConnection);
                    break;
                case METHODID_SET_CONNECTION:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.Connection) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionId>) responseObserver,
                            compression,
                            serviceImpl::setConnection);
                    break;
                case METHODID_REMOVE_CONNECTION:
                    io.quarkus.grpc.runtime.ServerCalls.oneToOne((context.ContextOuterClass.ConnectionId) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.Empty>) responseObserver,
                            compression,
                            serviceImpl::removeConnection);
                    break;
                case METHODID_GET_CONNECTION_EVENTS:
                    io.quarkus.grpc.runtime.ServerCalls.oneToMany((context.ContextOuterClass.Empty) request,
                            (io.grpc.stub.StreamObserver<context.ContextOuterClass.ConnectionEvent>) responseObserver,
                            compression,
                            serviceImpl::getConnectionEvents);
                    break;
                default:
                    throw new java.lang.AssertionError();
            }
        }

        @java.lang.Override
        @java.lang.SuppressWarnings("unchecked")
        public io.grpc.stub.StreamObserver<Req> invoke(io.grpc.stub.StreamObserver<Resp> responseObserver) {
            switch (methodId) {
                default:
                    throw new java.lang.AssertionError();
            }
        }
    }

}