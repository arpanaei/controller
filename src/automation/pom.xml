<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>eu.teraflow</groupId>
  <artifactId>automation</artifactId>
  <version>0.2.0</version>
  <name>TeraFlow Automation Component</name>
  <description>TeraFlow Automation Component</description>

  <properties>
    <java.version>11</java.version>
    <!-- https://maven.apache.org/plugins/maven-compiler-plugin/compile-mojo.html#parameters -->
    <maven.compiler.parameters>true</maven.compiler.parameters>
    <maven.compiler.source>${java.version}</maven.compiler.source>
    <maven.compiler.target>${java.version}</maven.compiler.target>
    <maven.version>3.8.1</maven.version>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

    <assertj.version>3.20.2</assertj.version>
    <quarkus.platform.artifact-id>quarkus-bom</quarkus.platform.artifact-id>
    <quarkus.platform.group-id>io.quarkus.platform</quarkus.platform.group-id>
    <jacoco.report.unit-test>jacoco-unit-tests.exec</jacoco.report.unit-test>
    <jacoco.report.integration-test>jacoco-integration-tests.exec</jacoco.report.integration-test>
    <jacoco.version>0.8.6</jacoco.version>
    <!-- Maven plugins -->
    <docker-maven-plugin.version>0.34.1</docker-maven-plugin.version>
    <maven-assembly-plugin.version>3.3.0</maven-assembly-plugin.version>
    <maven-clean-plugin.version>3.1.0</maven-clean-plugin.version>
    <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
    <maven-dependency-plugin.version>3.1.2</maven-dependency-plugin.version>
    <maven-deploy-plugin.version>3.0.0-M1</maven-deploy-plugin.version>
    <maven-enforcer-plugin.version>3.0.0-M3</maven-enforcer-plugin.version>
    <maven-install-plugin.version>3.0.0-M1</maven-install-plugin.version>
    <maven-jar-plugin.version>3.2.0</maven-jar-plugin.version>
    <maven-resources-plugin.version>3.2.0</maven-resources-plugin.version>
    <maven-surefire-plugin.version>3.0.0-M5</maven-surefire-plugin.version>
    <sonarsource-scanner-plugin.version>3.8.0.2131</sonarsource-scanner-plugin.version>
    <spotless-plugin.version>2.10.3</spotless-plugin.version>
    <versions-maven-plugin.version>2.8.1</versions-maven-plugin.version>
    <quarkus-maven-plugin.version>${quarkus.platform.version}</quarkus-maven-plugin.version>
    <!-- Third-party Dependencies -->
    <quarkus.platform.version>2.2.2.Final</quarkus.platform.version>
  </properties>

  <distributionManagement>
    <repository>
      <id>gitlab-maven</id>
      <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
    </repository>

    <snapshotRepository>
      <id>gitlab-maven</id>
      <url>${env.CI_SERVER_URL}/api/v4/projects/${env.CI_PROJECT_ID}/packages/maven</url>
    </snapshotRepository>
  </distributionManagement>

  <!-- Property resolution occurs only after parent POM has been resolved. Source: https://stackoverflow.com/a/59813598 -->
  <repositories>
    <!-- Explicitly define Maven central (first), so that CI job tries to fetch from it first. -->
    <repository>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
    </repository>
  </repositories>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>io.quarkus</groupId>
        <artifactId>quarkus-universe-bom</artifactId>
        <version>${quarkus.platform.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <dependencies>

    <dependency>
      <groupId>io.github.project-openubl</groupId>
      <artifactId>quarkus-bouncycastle</artifactId>
      <version>1.2.2.Final</version>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-grpc</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-kubernetes</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-container-image-jib</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-kubernetes-service-binding</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-smallrye-health</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-resteasy-reactive</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-arc</artifactId>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-config-yaml</artifactId>
    </dependency>

    <!-- Testing -->
    <dependency>
      <groupId>org.assertj</groupId>
      <artifactId>assertj-core</artifactId>
      <version>${assertj.version}</version>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>org.jacoco</groupId>
      <artifactId>org.jacoco.agent</artifactId>
      <classifier>runtime</classifier>
      <scope>test</scope>
      <version>${jacoco.version}</version>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-junit5</artifactId>
      <scope>test</scope>
    </dependency>

    <dependency>
      <groupId>io.quarkus</groupId>
      <artifactId>quarkus-junit5-mockito</artifactId>
      <scope>test</scope>
    </dependency>

  </dependencies>

  <build>
      <plugins>

        <plugin>
          <artifactId>maven-assembly-plugin</artifactId>
          <version>${maven-assembly-plugin.version}</version>
        </plugin>

        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>${maven-clean-plugin.version}</version>
        </plugin>

        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
          <configuration>
            <source>${java.version}</source>
            <target>${java.version}</target>
            <compilerArgs>
              <arg>-Xlint:deprecation</arg>
            </compilerArgs>
          </configuration>
        </plugin>

        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>${maven-deploy-plugin.version}</version>
        </plugin>

        <plugin>
          <artifactId>maven-failsafe-plugin</artifactId>
          <version>${maven-surefire-plugin.version}</version>
          <configuration>
            <systemPropertyVariables>
              <jacoco-agent.destfile>${project.build.directory}/${jacoco.report.integration-test}</jacoco-agent.destfile>
            </systemPropertyVariables>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>integration-test</goal>
                <goal>verify</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>${maven-install-plugin.version}</version>
        </plugin>

        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <version>${maven-jar-plugin.version}</version>
          <configuration>
            <archive>
              <manifest>
                <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
              </manifest>
            </archive>
          </configuration>
        </plugin>

        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>${maven-resources-plugin.version}</version>
        </plugin>

        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>${maven-surefire-plugin.version}</version>
          <configuration>
            <skipTests>${skipUTs}</skipTests>
            <systemPropertyVariables>
              <jacoco-agent.destfile>${project.build.directory}/${jacoco.report.unit-test}</jacoco-agent.destfile>
            </systemPropertyVariables>
          </configuration>
        </plugin>

        <plugin>
          <groupId>org.sonarsource.scanner.maven</groupId>
          <artifactId>sonar-maven-plugin</artifactId>
          <version>${sonarsource-scanner-plugin.version}</version>
        </plugin>

        <!-- https://quarkus.io/guides/maven-tooling#logging-quarkus-application-build-classpath-tree -->
        <plugin>
          <groupId>io.quarkus</groupId>
          <artifactId>quarkus-bootstrap-maven-plugin</artifactId>
          <version>${quarkus-maven-plugin.version}</version>
        </plugin>

        <plugin>
          <groupId>io.quarkus</groupId>
          <artifactId>quarkus-maven-plugin</artifactId>
          <version>${quarkus-maven-plugin.version}</version>
          <extensions>true</extensions>
          <executions>
            <execution>
              <goals>
                <goal>build</goal>
                <goal>generate-code</goal>
                <goal>generate-code-tests</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <groupId>org.jacoco</groupId>
          <artifactId>jacoco-maven-plugin</artifactId>
          <version>${jacoco.version}</version>
          <configuration>
          <excludes>
            <exclude>automation/*</exclude>
            <exclude>context/*</exclude>
            <exclude>device/*</exclude>
            <exclude>monitoring/*</exclude>
            <exclude>kpi_sample_types/*</exclude>
          </excludes>
          </configuration>
          <executions>
            <execution>
              <id>instrument-unit-tests</id>
              <phase>process-classes</phase>
              <goals>
                <goal>instrument</goal>
              </goals>
            </execution>
            <execution>
              <id>restore-unit-tests</id>
              <phase>test</phase>
              <goals>
                <goal>restore-instrumented-classes</goal>
              </goals>
            </execution>
            <execution>
              <id>report-unit-tests</id>
              <phase>test</phase>
              <goals>
                <goal>report</goal>
              </goals>
              <configuration>
                <dataFile>${project.build.directory}/${jacoco.report.unit-test}</dataFile>
                <outputDirectory>${project.reporting.outputDirectory}/jacoco-unit-tests</outputDirectory>
              </configuration>
            </execution>

            <execution>
              <id>instrument-integration-tests</id>
              <phase>pre-integration-test</phase>
              <goals>
                <goal>instrument</goal>
              </goals>
            </execution>
            <execution>
              <id>restore-integration-tests</id>
              <phase>post-integration-test</phase>
              <goals>
                <goal>restore-instrumented-classes</goal>
              </goals>
            </execution>

            <execution>
              <id>report-integration-tests</id>
              <phase>post-integration-test</phase>
              <goals>
                <goal>report</goal>
              </goals>
              <configuration>
                <dataFile>${project.build.directory}/${jacoco.report.integration-test}</dataFile>
                <outputDirectory>${project.reporting.outputDirectory}/jacoco-integration-tests</outputDirectory>
              </configuration>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <artifactId>maven-dependency-plugin</artifactId>
          <version>${maven-dependency-plugin.version}</version>
          <executions>
            <execution>
              <id>analyze-dependencies</id>
              <goals>
                <goal>analyze-only</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <artifactId>maven-enforcer-plugin</artifactId>
          <version>${maven-enforcer-plugin.version}</version>
          <configuration>
            <rules>
              <requireJavaVersion>
                <version>${java.version}</version>
              </requireJavaVersion>
              <requireMavenVersion>
                <version>${maven.version}</version>
              </requireMavenVersion>
            </rules>
          </configuration>
          <executions>
            <execution>
              <goals>
                <goal>enforce</goal>
              </goals>
            </execution>
          </executions>
        </plugin>

        <plugin>
          <groupId>com.diffplug.spotless</groupId>
          <artifactId>spotless-maven-plugin</artifactId>
          <version>${spotless-plugin.version}</version>
          <configuration>
            <java>
              <!--
                  Allow toggling spotless, as per:
                  https://github.com/diffplug/spotless/tree/main/plugin-maven#spotlessoff-and-spotlesson
              -->
              <toggleOffOn>
                <off>fmt:off</off>
                <on>fmt:on</on>
              </toggleOffOn>

              <googleJavaFormat>
                <version>1.10.0</version>
                <style>GOOGLE</style>
              </googleJavaFormat>

              <!-- Custom configuration -->
              <indent>
                <tabs>true</tabs>
                <spacesPerTab>2</spacesPerTab>
              </indent>
              <indent>
                <spaces>true</spaces>
                <spacesPerTab>4</spacesPerTab>
              </indent>
            </java>
          </configuration>
          <executions>
            <execution>
              <phase>compile</phase>
              <goals>
                <goal>apply</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
      </plugins>
  </build>

  <profiles>
    <profile>
      <id>generate-consolidated-coverage</id>
      <activation>
        <activeByDefault>false</activeByDefault>
      </activation>
      <build>
        <plugins>
          <plugin>
            <groupId>org.jacoco</groupId>
            <artifactId>jacoco-maven-plugin</artifactId>
            <executions>

              <!--
                Build consolidated coverage report as per
                https://quarkus.io/version/1.11/guides/tests-with-coverage#instrumenting-the-classes-instead
              -->
              <execution>
                <id>merge-results</id>
                <phase>verify</phase>
                <goals>
                  <goal>merge</goal>
                </goals>
                <configuration>
                  <fileSets>
                    <fileSet>
                      <directory>${project.build.directory}</directory>
                      <includes>
                        <include>*.exec</include>
                      </includes>
                    </fileSet>
                  </fileSets>
                  <destFile>${project.build.directory}/jacoco.exec</destFile>
                </configuration>
              </execution>

              <execution>
                <id>post-merge-report</id>
                <phase>verify</phase>
                <goals>
                  <goal>report</goal>
                </goals>
                <configuration>
                  <dataFile>${project.build.directory}/jacoco.exec</dataFile>
                  <outputDirectory>${project.reporting.outputDirectory}/jacoco</outputDirectory>
                </configuration>

              </execution>
            </executions>
          </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
</project>
