# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os, sys, logging
from prometheus_client import start_http_server
from common.Settings import wait_for_environment_variables
from webui.service import create_app
from webui.Config import WEBUI_SERVICE_PORT, LOG_LEVEL, METRICS_PORT, HOST, SECRET_KEY, DEBUG

def main():
    service_port = os.environ.get('WEBUISERVICE_SERVICE_PORT', WEBUI_SERVICE_PORT)
    log_level    = os.environ.get('LOG_LEVEL',                 LOG_LEVEL         )
    metrics_port = os.environ.get('METRICS_PORT',              METRICS_PORT      )
    host         = os.environ.get('HOST',                      HOST              )
    debug        = os.environ.get('DEBUG',                     DEBUG             )

    logging.basicConfig(level=log_level)
    logger = logging.getLogger(__name__)

    wait_for_environment_variables([
        'CONTEXTSERVICE_SERVICE_HOST', 'CONTEXTSERVICE_SERVICE_PORT_GRPC',
        'DEVICESERVICE_SERVICE_HOST', 'DEVICESERVICE_SERVICE_PORT_GRPC'
    ])

    logger.info('Starting...')

    start_http_server(metrics_port)

    app = create_app(use_config={'SECRET_KEY': SECRET_KEY})
    app.run(host=host, port=service_port, debug=debug)

    logger.info('Bye')
    return 0

if __name__ == '__main__':
    sys.exit(main())
