# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import logging

# General settings
LOG_LEVEL = logging.DEBUG

# gRPC settings
WEBUI_SERVICE_PORT = 8004

# Prometheus settings
METRICS_PORT = 9192

SECRET_KEY = '>s&}24@{]]#k3&^5$f3#?6?h3{W@[}/7z}2pa]>{3&5%RP<)[('

HOST = '0.0.0.0'  # accepts connections coming from any ADDRESS

DEBUG=False

CONTEXT_SERVICE_ADDRESS = os.environ.get('CONTEXTSERVICE_SERVICE_HOST', 'contextservice')
CONTEXT_SERVICE_PORT = int(os.environ.get('CONTEXTSERVICE_SERVICE_PORT_GRPC', 1010))

DEVICE_SERVICE_ADDRESS = os.environ.get('DEVICESERVICE_SERVICE_HOST', 'deviceservice')
DEVICE_SERVICE_PORT = int(os.environ.get('DEVICESERVICE_SERVICE_PORT_GRPC', 2020))
