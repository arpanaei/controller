# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import grpc

from monitoring.proto import monitoring_pb2
from monitoring.proto import monitoring_pb2_grpc
from monitoring.proto import context_pb2

from common.logger import getJSONLogger
LOGGER = getJSONLogger('monitoring-client')
LOGGER.setLevel('DEBUG')

class MonitoringClient:

    def __init__(self, server='monitoring', port='7070'):
        endpoint = '{}:{}'.format(server, port)
        LOGGER.info('init monitoringClient {}'.format(endpoint))
        self.channel = grpc.insecure_channel(endpoint)
        self.server = monitoring_pb2_grpc.MonitoringServiceStub(self.channel)

    def CreateKpi(self, request):
        LOGGER.info('CreateKpi: {}'.format(request))
        response = self.server.CreateKpi(request)
        LOGGER.info('CreateKpi result: {}'.format(response))
        return response

    def MonitorKpi(self, request):
        LOGGER.info('MonitorKpi: {}'.format(request))
        response = self.server.MonitorKpi(request)
        LOGGER.info('MonitorKpi result: {}'.format(response))
        return response

    def IncludeKpi(self, request):
        LOGGER.info('IncludeKpi: {}'.format(request))
        response = self.server.IncludeKpi(request)
        LOGGER.info('IncludeKpi result: {}'.format(response))
        return response

    def GetStreamKpi(self, request):
        LOGGER.info('GetStreamKpi: {}'.format(request))
        response = self.server.GetStreamKpi(request)
        LOGGER.info('GetStreamKpi result: {}'.format(response))
        yield monitoring_pb2.Kpi()

    def GetInstantKpi(self, request):
        LOGGER.info('GetInstantKpi: {}'.format(request))
        response = self.server.GetInstantKpi(request)
        LOGGER.info('GetInstantKpi result: {}'.format(response))
        return monitoring_pb2.Kpi()

    def GetKpiDescriptor(self, request):
        LOGGER.info('GetKpiDescriptor: {}'.format(request))
        response = self.server.GetKpiDescriptor(request)
        LOGGER.info('GetKpiDescriptor result: {}'.format(response))
        return response

if __name__ == '__main__':
    # get port
    port = sys.argv[1] if len(sys.argv) > 1 else '7070'

    # make call to server
    client = MonitoringClient(port=port)
