# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from typing import Dict, List
from common.orm.fields.PrimaryKeyField import PrimaryKeyField
from common.orm.fields.StringField import StringField
from common.orm.model.Model import Model

LOGGER = logging.getLogger(__name__)

class ContextModel(Model):
    pk = PrimaryKeyField()
    context_uuid = StringField(required=True, allow_empty=False)

    def dump_id(self) -> Dict:
        return {'context_uuid': {'uuid': self.context_uuid}}

    def dump_service_ids(self) -> List[Dict]:
        from .ServiceModel import ServiceModel # pylint: disable=import-outside-toplevel
        db_service_pks = self.references(ServiceModel)
        return [ServiceModel(self.database, pk).dump_id() for pk,_ in db_service_pks]

    def dump_topology_ids(self) -> List[Dict]:
        from .TopologyModel import TopologyModel # pylint: disable=import-outside-toplevel
        db_topology_pks = self.references(TopologyModel)
        return [TopologyModel(self.database, pk).dump_id() for pk,_ in db_topology_pks]

    def dump(self, include_services=True, include_topologies=True) -> Dict: # pylint: disable=arguments-differ
        result = {'context_id': self.dump_id()}
        if include_services: result['service_ids'] = self.dump_service_ids()
        if include_topologies: result['topology_ids'] = self.dump_topology_ids()
        return result
