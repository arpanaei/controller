# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM python:3-slim

# Install dependencies
RUN apt-get --yes --quiet --quiet update && \
    apt-get --yes --quiet --quiet install wget g++ && \
    rm -rf /var/lib/apt/lists/*

# Set Python to show logs as they occur
ENV PYTHONUNBUFFERED=0

# Download the gRPC health probe
RUN GRPC_HEALTH_PROBE_VERSION=v0.2.0 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

# Get generic Python packages
RUN python3 -m pip install --upgrade pip setuptools wheel pip-tools

# Set working directory
WORKDIR /var/teraflow

# Create module sub-folders
RUN mkdir -p /var/teraflow/opticalcentralizedattackdetector

# Get Python packages per module
COPY opticalcentralizedattackdetector/requirements.in opticalcentralizedattackdetector/requirements.in
RUN pip-compile --output-file=opticalcentralizedattackdetector/requirements.txt opticalcentralizedattackdetector/requirements.in
RUN python3 -m pip install -r opticalcentralizedattackdetector/requirements.txt

# Add files into working directory
COPY common/. common
COPY context/. context
COPY monitoring/. monitoring
COPY service/. service
COPY dbscanserving/. dbscanserving
COPY opticalattackmitigator/. opticalattackmitigator
COPY opticalcentralizedattackdetector/. opticalcentralizedattackdetector

ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python

# Start opticalcentralizedattackdetector service
ENTRYPOINT ["python", "-m", "opticalcentralizedattackdetector.service"]
