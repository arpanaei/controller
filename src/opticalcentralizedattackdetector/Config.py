# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging

# General settings
LOG_LEVEL = logging.DEBUG

# gRPC settings
GRPC_SERVICE_PORT = 10005
GRPC_MAX_WORKERS  = 10
GRPC_GRACE_PERIOD = 60

# service settings
MONITORING_INTERVAL = 2  # monitoring interval in seconds
#TODO: adjust the addresses below for the specific case
MONITORING_SERVICE_ADDRESS = 'monitoringservice'  # address/name of the monitoring service
# MONITORING_SERVICE_ADDRESS = '10.99.41.20'  # address/name of the monitoring service
CONTEXT_SERVICE_ADDRESS = 'contextservice'  # address/name of the monitoring service
# CONTEXT_SERVICE_ADDRESS = '10.107.199.65'  # address/name of the monitoring service
SERVICE_SERVICE_ADDRESS = 'serviceservice'  # address/name of the service service
# SERVICE_SERVICE_ADDRESS = '10.99.234.88'  # address/name of the service service
# INFERENCE_SERVICE_ADDRESS = '10.108.113.78'  # address/name of the inference service
INFERENCE_SERVICE_ADDRESS = 'dbscanservingservice'  # address/name of the inference service
# ATTACK_MITIGATOR_SERVICE_ADDRESS = '10.96.248.167'
ATTACK_MITIGATOR_SERVICE_ADDRESS = 'opticalattackmitigatorservice'

# Prometheus settings
METRICS_PORT = 9192
