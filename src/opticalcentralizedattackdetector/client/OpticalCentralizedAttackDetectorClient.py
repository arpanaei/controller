# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import grpc, logging
from common.tools.client.RetryDecorator import retry, delay_exponential
from opticalcentralizedattackdetector.proto.context_pb2 import Empty, Service
from opticalcentralizedattackdetector.proto.monitoring_pb2 import KpiList
from opticalcentralizedattackdetector.proto.optical_centralized_attack_detector_pb2_grpc import OpticalCentralizedAttackDetectorServiceStub

LOGGER = logging.getLogger(__name__)
MAX_RETRIES = 15
DELAY_FUNCTION = delay_exponential(initial=0.01, increment=2.0, maximum=5.0)
RETRY_DECORATOR = retry(max_retries=MAX_RETRIES, delay_function=DELAY_FUNCTION, prepare_method_name='connect')

class OpticalCentralizedAttackDetectorClient:
    def __init__(self, address, port):
        self.endpoint = '{:s}:{:s}'.format(str(address), str(port))
        LOGGER.debug('Creating channel to {:s}...'.format(str(self.endpoint)))
        self.channel = None
        self.stub = None
        self.connect()
        LOGGER.debug('Channel created')

    def connect(self):
        self.channel = grpc.insecure_channel(self.endpoint)
        self.stub = OpticalCentralizedAttackDetectorServiceStub(self.channel)

    def close(self):
        if(self.channel is not None): self.channel.close()
        self.channel = None
        self.stub = None

    @RETRY_DECORATOR
    def NotifyServiceUpdate(self, request : Service) -> Empty:
        LOGGER.debug('NotifyServiceUpdate request: {:s}'.format(str(request)))
        response = self.stub.NotifyServiceUpdate(request)
        LOGGER.debug('NotifyServiceUpdate result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def DetectAttack(self, request : Empty) -> Empty:
        LOGGER.debug('DetectAttack request: {:s}'.format(str(request)))
        response = self.stub.DetectAttack(request)
        LOGGER.debug('DetectAttack result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def ReportSummarizedKpi(self, request : KpiList) -> Empty:
        LOGGER.debug('ReportSummarizedKpi request: {:s}'.format(str(request)))
        response = self.stub.ReportSummarizedKpi(request)
        LOGGER.debug('ReportSummarizedKpi result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def ReportKpi(self, request : KpiList) -> Empty:
        LOGGER.debug('ReportKpi request: {:s}'.format(str(request)))
        response = self.stub.ReportKpi(request)
        LOGGER.debug('ReportKpi result: {:s}'.format(str(response)))
        return response
