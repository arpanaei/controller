```
     NEC Laboratories Europe GmbH

     PROPRIETARY INFORMATION

 The software and its source code contain valuable trade secrets and
 shall be maintained in confidence and treated as confidential
 information. The software may only be used for evaluation and/or
 testing purposes, unless otherwise explicitly stated in a written
 agreement with NEC Laboratories Europe GmbH.

 Any unauthorized publication, transfer to third parties or
 duplication of the object or source code - either totally or in
 part - is strictly prohibited.

          Copyright (c) 2021 NEC Laboratories Europe GmbH
          All Rights Reserved.

 Authors: Konstantin Munichev <konstantin.munichev@neclab.eu>


 NEC Laboratories Europe GmbH DISCLAIMS ALL WARRANTIES, EITHER
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AND THE
 WARRANTY AGAINST LATENT DEFECTS, WITH RESPECT TO THE PROGRAM AND
 THE ACCOMPANYING DOCUMENTATION.

 NO LIABILITIES FOR CONSEQUENTIAL DAMAGES: IN NO EVENT SHALL NEC
 Laboratories Europe GmbH or ANY OF ITS SUBSIDIARIES BE LIABLE FOR
 ANY DAMAGES WHATSOEVER (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR
 LOSS OF BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF
 INFORMATION, OR OTHER PECUNIARY LOSS AND INDIRECT, CONSEQUENTIAL,
 INCIDENTAL, ECONOMIC OR PUNITIVE DAMAGES) ARISING OUT OF THE USE OF
 OR INABILITY TO USE THIS PROGRAM, EVEN IF NEC Laboratories Europe
 GmbH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

 THIS HEADER MAY NOT BE EXTRACTED OR MODIFIED IN ANY WAY.
 ```

# DLT module guide

## General information
The DLT module is used to provide access to the underlying Fabric deployment. It allows clients
to add, retrieve, modify and delete blockchain-backed data, essentially working as a key-value
database. External clients should use REST API to communicate with this service, its detailed
description available below.

## Code structure
The whole DLT module consists of several packages:
- fabric package
- http package
- proto package
- client example

### Fabric package
The most important class in this package is `FabricConnector`. First, it establishes connection
with the underlying Fabric network using Java Gateway SDK. After that, it could be used as a
CRUD interface.
Other files contain auxiliary code for `FabricConnector` which allows it to register/enroll
users and to obtain smart contract instances.

### HTTP package
Contains server side HTTP handler. It accepts requests from the outside and performs the
requested operation. For the detailed description see API description section.

### Proto package
The proto package contains `Config.proto` file which contains messages for REST API. The most
important ones are `DltConfig` (it defines the whole DLT configuration) and `DltRecord` which
represents data to store in the blockchain.

### Client example
This code is not necessary to the service, but it could be used to test the service. It contains
a sample REST client which connects the service and perform all the CRUD operations. 

## REST API description
| Method | URL | Input | Response code | Output |
| --- | ----------- | --- | --- | --- |
| POST | /dlt/configure | Configuration object | 201 or 400 | Status value | 
| GET | /dlt/configure | - | 200 or 404 | Configuration object |
| POST | /dlt/record | Record object | 200, 201, 400 or 404 | Status value |
| GET | /dlt/record | Record id | 200 or 404 | Record object |

Record and configuration object are defined in `proto` package.

