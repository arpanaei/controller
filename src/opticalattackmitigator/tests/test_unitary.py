# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging, pytest
from opticalattackmitigator.Config import GRPC_SERVICE_PORT, GRPC_MAX_WORKERS, GRPC_GRACE_PERIOD
from opticalattackmitigator.client.OpticalAttackMitigatorClient import OpticalAttackMitigatorClient
from opticalattackmitigator.service.OpticalAttackMitigatorService import OpticalAttackMitigatorService
from opticalattackmitigator.proto.optical_attack_mitigator_pb2 import AttackDescription, AttackResponse

port = 10000 + GRPC_SERVICE_PORT # avoid privileged ports

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

@pytest.fixture(scope='session')
def optical_attack_mitigator_service():
    _service = OpticalAttackMitigatorService(
        port=port, max_workers=GRPC_MAX_WORKERS, grace_period=GRPC_GRACE_PERIOD)
    _service.start()
    yield _service
    _service.stop()

@pytest.fixture(scope='session')
def optical_attack_mitigator_client(optical_attack_mitigator_service):
    _client = OpticalAttackMitigatorClient(address='127.0.0.1', port=port)
    yield _client
    _client.close()

def test_call_service(optical_attack_mitigator_client: OpticalAttackMitigatorClient):
    request = AttackDescription()
    optical_attack_mitigator_client.NotifyAttack(request)
