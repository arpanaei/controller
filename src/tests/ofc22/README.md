# OFC'22 Demo - Bootstrap devices, Monitor device Endpoints, Manage L3VPN Services
This functional test reproduces the live demonstration "Demonstration of Zero-touch Device and L3-VPN Service
Management Using the TeraFlow Cloud-native SDN Controller" carried out at
[OFC'22](https://www.ofcconference.org/en-us/home/program-speakers/demo/).

## Functional test folder
This functional test can be found in folder `./src/tests/ofc22/`. A convenience alias `./ofc22/` pointing to that folder has been defined.

## Execute with real devices
This functional test is designed to operate both with real and emulated devices.
By default, emulated devices are used; however, if you have access to real devices, you can create/modify the files `./ofc22/tests/Objects.py` and `./ofc22/tests/Credentials.py` to point to your devices, and map to your network topology.
Note that the default scenario assumes devices R2 and R4 are always emulated, while devices R1, R3, and O1 can be configured as emulated or real devices.

__Important:__ The OpenConfigDriver, the P4Driver, and the TrandportApiDriver have to be considered as experimental. The configuration and monitoring capabilities they support are limited or partially implemented. Use them with care.

## Deployment
To run this functional test, it is assumed you have deployed a Kubernetes-based environment as described in [Wiki: Installing Kubernetes on your Linux machine](https://gitlab.com/teraflow-h2020/controller/-/wikis/Installing-Kubernetes-on-your-Linux-machine).

After installing Kubernetes, you can run it to deploy the appropriate components. Feel free to adapt it your particular case following the instructions described in [Wiki: Deploying a TeraFlow OS test instance](https://gitlab.com/teraflow-h2020/controller/-/wikis/Deploying-a-TeraFlow-OS-test-instance).

__Important:__
- The `./ofc22/deploy_in_kubernetes.sh` assumes you have installed the appropriate development dependencies using the `install_development_dependencies.sh` script.
- Before running the scripts in this folder, remember to update the environment variable K8S_HOSTNAME to point to the Kubernetes node you will be using as described in [Wiki: Deploying a TeraFlow OS test instance](https://gitlab.com/teraflow-h2020/controller/-/wikis/Deploying-a-TeraFlow-OS-test-instance).

For your convenience, the configuration s sript `./ofc22/deploy_in_kubernetes.sh` has been already defined. The script will take some minutes to download the dependencies, build the micro-services, deploy them, and leave them ready for operation. The deployment will finish with a report of the items that have been created.

## Access to the WebUI and Dashboard
When the deployment completes, you can connect to the TeraFlow OS WebUI and Dashboards as described in [Wiki: Using the WebUI](https://gitlab.com/teraflow-h2020/controller/-/wikis/Using-the-WebUI), or directly navigating to `http://[your-node-ip]:30800` for the WebUI and `http://[your-node-ip]:30300` for the Grafana Dashboard.

Notes:
- the default credentials for the Grafana Dashboiard is user/pass: `admin`/`admin123+`.
- in Grafana, you can find the "L3-Monitorng" in the "Starred dashboards" section.

## Test execution
To execute this functional test, four main steps needs to be carried out:
1. Device bootstrapping
2. L3VPN Service creation
3. L3VPN Service removal
4. Cleanup

Upon the execution of each test progresses, a report will be generated indicating PASSED / FAILED / SKIPPED. If there is some error during the execution, you should see a detailed report on the error. See the troubleshooting section in that case.

Feel free to check the logs of the different components using the appropriate `ofc22/show_logs_[component].sh` scripts after you execute each step.

### 1. Device bootstrapping

This step configures some basic entities (Context and Topology), the devices, and the links in the topology. The expected results are:
- The devices to be incorporated into the Topology.
- The devices to be pre-configured and initialized as ENABLED by the Automation component.
- The monitoring for the device ports (named as endpoints in TeraFlow OS) to be activated and data collection to automatically start.
- The links to be added to the topology.

To run this step, execute the following script:
`./ofc22/run_test_01_bootstrap.sh`

When the script finishes, check in the Grafana L3-Monitoring Dashboard and you should see the monitoring data being plotted and updated every 5 seconds (by default). Given that there is no service configured, you should see a 0-valued flat plot.

In the WebUI, select the "admin" Context. In the "Devices" tab you should see that 5 different emulated devices have been created and activated: 4 packet routers, and 1 optical line system controller. Besides, in the "Services" tab you should see that there is no service created. Note here that the emulated devices produce synthetic randomly-generated data and do not care about the services configured.

### 2. L3VPN Service creation

This step configures a new service emulating the request an OSM WIM would make by means of a Mock OSM instance.

To run this step, execute the following script:
`./ofc22/run_test_02_create_service.sh`

When the script finishes, check the WebUI "Services" tab. You should see that two services have been created, one for the optical layer and another for the packet layer. Besides, you can check the "Devices" tab to see the configuration rules that have been configured in each device. In the Grafana Dashboard, given that there is now a service configured, you should see the plots with the monitored data for the device. By default, device R1-INF is selected.

### 3. L3VPN Service removal

This step deconfigures the previously created services emulating the request an OSM WIM would make by means of a Mock OSM instance.

To run this step, execute the following script:
`./ofc22/run_test_03_delete_service.sh`

When the script finishes, check the WebUI "Services" tab. You should see that the two services have been removed. Besides, in the "Devices" tab you can see that the appropriate configuration rules have been deconfigured. In the Grafana Dashboard, given that there is no service configured, you should see a 0-valued flat plot again.

### 4. Cleanup

This last step just performs a cleanup of the scenario removing all the TeraFlow OS entities for completeness.

To run this step, execute the following script:
`./ofc22/run_test_04_cleanup.sh`

When the script finishes, check the WebUI "Devices" tab, you should see that the devices have been removed. Besides, in the "Services" tab you can see that the "admin" Context has no services given that that context has been removed.

## Troubleshooting

Different scripts are provided to help in troubleshooting issues in the execution of the test. These scripts are:
- `./ofc22/show_deployment.sh`: this script reports the items belonging to this deployment. Use it to validate that all the pods, deployments and replica sets are ready and have a state of "running"; and the services are deployed and have appropriate IP addresses and ports.
- `ofc22/show_logs_automation.sh`: this script reports the logs for the automation component.
- `ofc22/show_logs_compute.sh`: this script reports the logs for the compute component.
- `ofc22/show_logs_context.sh`: this script reports the logs for the context component.
- `ofc22/show_logs_device.sh`: this script reports the logs for the device component.
- `ofc22/show_logs_monitoring.sh`: this script reports the logs for the monitoring component.
- `ofc22/show_logs_service.sh`: this script reports the logs for the service component.
- `ofc22/show_logs_webui.sh`: this script reports the logs for the webui component.
