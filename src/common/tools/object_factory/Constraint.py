# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from typing import Any, Dict, Union

def json_constraint(constraint_type : str, constraint_value : Union[str, Dict[str, Any]]):
    if not isinstance(constraint_value, str): constraint_value = json.dumps(constraint_value, sort_keys=True)
    return {'constraint_type': constraint_type, 'constraint_value': constraint_value}
