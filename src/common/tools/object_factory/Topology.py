# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import copy
from typing import Dict, Optional

def json_topology_id(topology_uuid : str, context_id : Optional[Dict] = None):
    result = {'topology_uuid': {'uuid': topology_uuid}}
    if context_id is not None: result['context_id'] = copy.deepcopy(context_id)
    return result

def json_topology(topology_uuid : str, context_id : Optional[Dict] = None):
    return {
        'topology_id': json_topology_id(topology_uuid, context_id=context_id),
        'device_ids' : [],
        'link_ids'   : [],
    }
