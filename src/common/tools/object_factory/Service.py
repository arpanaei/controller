# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import copy
from typing import Dict, List, Optional
from common.Constants import DEFAULT_CONTEXT_UUID
from common.tools.object_factory.Context import json_context_id
from context.proto.context_pb2 import ServiceStatusEnum, ServiceTypeEnum

def json_service_id(service_uuid : str, context_id : Optional[Dict] = None):
    result = {'service_uuid': {'uuid': service_uuid}}
    if context_id is not None: result['context_id'] = copy.deepcopy(context_id)
    return result

def json_service(
    service_uuid : str, service_type : ServiceTypeEnum, context_id : Optional[Dict] = None,
    status : ServiceStatusEnum = ServiceStatusEnum.SERVICESTATUS_PLANNED,
    endpoint_ids : List[Dict] = [], constraints : List[Dict] = [], config_rules : List[Dict] = []):

    return {
        'service_id'          : json_service_id(service_uuid, context_id=context_id),
        'service_type'        : service_type,
        'service_status'      : {'service_status': status},
        'service_endpoint_ids': copy.deepcopy(endpoint_ids),
        'service_constraints' : copy.deepcopy(constraints),
        'service_config'      : {'config_rules': copy.deepcopy(config_rules)},
    }

def json_service_l3nm_planned(
        service_uuid : str, endpoint_ids : List[Dict] = [], constraints : List[Dict] = [],
        config_rules : List[Dict] = []
    ):

    return json_service(
        service_uuid, ServiceTypeEnum.SERVICETYPE_L3NM, context_id=json_context_id(DEFAULT_CONTEXT_UUID),
        status=ServiceStatusEnum.SERVICESTATUS_PLANNED, endpoint_ids=endpoint_ids, constraints=constraints,
        config_rules=config_rules)
