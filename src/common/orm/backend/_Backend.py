# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Any, Dict, List, Optional, Set, Tuple

class _Backend:
    def __init__(self, **settings) -> None:
        raise NotImplementedError()

    def lock(self, keys : List[List[str]], owner_key : Optional[str] = None) -> Tuple[bool, str]:
        raise NotImplementedError()

    def unlock(self, keys : List[List[str]], owner_key : str) -> bool:
        raise NotImplementedError()

    def keys(self) -> list:
        raise NotImplementedError()

    def exists(self, key : List[str]) -> bool:
        raise NotImplementedError()

    def delete(self, key : List[str]) -> bool:
        raise NotImplementedError()

    def dict_get(self, key : List[str], fields : List[str] = []) -> Dict[str, str]:
        raise NotImplementedError()

    def dict_update(self, key : List[str], fields : Dict[str, str] = {}) -> None:
        raise NotImplementedError()

    def dict_delete(self, key : List[str], fields : List[str] = []) -> None:
        raise NotImplementedError()

    def list_get_all(self, key : List[str]) -> List[str]:
        raise NotImplementedError()

    def list_push_last(self, key : List[str], item : str) -> None:
        raise NotImplementedError()

    def list_remove_first_occurrence(self, key : List[str], item: str) -> None:
        raise NotImplementedError()

    def set_add(self, key : List[str], item : str) -> None:
        raise NotImplementedError()

    def set_has(self, key : List[str], item : str) -> bool:
        raise NotImplementedError()

    def set_get_all(self, key : List[str]) -> Set[str]:
        raise NotImplementedError()

    def set_remove(self, key : List[str], item : str) -> None:
        raise NotImplementedError()

    def dump(self) -> List[Tuple[str, str, Any]]:
        raise NotImplementedError()
