# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Dict, List, Set, Union

def get_dict(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> Dict:
    return keys.get(str_key, None)

def get_or_create_dict(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> Dict:
    container = keys.get(str_key, None)
    if container is None: container = keys.setdefault(str_key, dict())
    if not isinstance(container, dict):
        raise Exception('Key({:s}, {:s}) is not a dict'.format(str(type(container).__name__), str(str_key)))
    return container

def get_list(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> List:
    return keys.get(str_key, None)

def get_or_create_list(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> List:
    container = keys.get(str_key, None)
    if container is None: container = keys.setdefault(str_key, list())
    if not isinstance(container, list):
        raise Exception('Key({:s}, {:s}) is not a list'.format(str(type(container).__name__), str(str_key)))
    return container

def get_set(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> Set:
    return keys.get(str_key, None)

def get_or_create_set(keys : Dict[str, Union[Dict, List, Set]], str_key : str) -> Set:
    container = keys.get(str_key, None)
    if container is None: container = keys.setdefault(str_key, set())
    if not isinstance(container, set):
        raise Exception('Key({:s}, {:s}) is not a set'.format(str(type(container).__name__), str(str_key)))
    return container
