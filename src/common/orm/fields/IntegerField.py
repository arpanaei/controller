# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations
from typing import Optional, Union
from common.type_checkers.Checkers import chk_integer
from .Field import Field

class IntegerField(Field):
    def __init__(
        self, *args, min_value : Optional[int] = None, max_value : Optional[int] = None, **kwargs) -> None:

        super().__init__(*args, type_=int, **kwargs)
        self._min_value = None if min_value is None else \
            chk_integer('IntegerField.min_value', min_value)
        self._max_value = None if max_value is None else \
            chk_integer('IntegerField.max_value', max_value, min_value=self._min_value)

    def validate(self, value : Union[int, str], try_convert_type=False) -> int:
        value = super().validate(value)
        if value is None: return None
        if try_convert_type and isinstance(value, str): value = int(value)
        return chk_integer(self.name, value, min_value=self._min_value, max_value=self._max_value)
