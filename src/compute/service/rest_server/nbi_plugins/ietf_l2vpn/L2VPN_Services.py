# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from typing import Dict, List
from flask import request
from flask.json import jsonify
from flask_restful import Resource
from werkzeug.exceptions import UnsupportedMediaType
from common.Constants import DEFAULT_CONTEXT_UUID
from common.Settings import get_setting
from service.client.ServiceClient import ServiceClient
from service.proto.context_pb2 import Service, ServiceStatusEnum, ServiceTypeEnum
from .schemas.vpn_service import SCHEMA_VPN_SERVICE
from .tools.Authentication import HTTP_AUTH
from .tools.HttpStatusCodes import HTTP_CREATED, HTTP_SERVERERROR
from .tools.Validator import validate_message

LOGGER = logging.getLogger(__name__)

class L2VPN_Services(Resource):
    def __init__(self) -> None:
        super().__init__()
        self.service_client = ServiceClient(
            get_setting('SERVICESERVICE_SERVICE_HOST'), get_setting('SERVICESERVICE_SERVICE_PORT_GRPC'))

    @HTTP_AUTH.login_required
    def get(self):
        return {}

    @HTTP_AUTH.login_required
    def post(self):
        if not request.is_json: raise UnsupportedMediaType('JSON payload is required')
        request_data : Dict = request.json
        LOGGER.debug('Request: {:s}'.format(str(request_data)))
        validate_message(SCHEMA_VPN_SERVICE, request_data)

        vpn_services : List[Dict] = request_data['ietf-l2vpn-svc:vpn-service']
        for vpn_service in vpn_services:
            # pylint: disable=no-member
            service_request = Service()
            service_request.service_id.context_id.context_uuid.uuid = DEFAULT_CONTEXT_UUID
            service_request.service_id.service_uuid.uuid = vpn_service['vpn-id']
            service_request.service_type = ServiceTypeEnum.SERVICETYPE_L3NM
            service_request.service_status.service_status = ServiceStatusEnum.SERVICESTATUS_PLANNED

            try:
                service_reply = self.service_client.CreateService(service_request)
                if service_reply != service_request.service_id: # pylint: disable=no-member
                    raise Exception('Service creation failed. Wrong Service Id was returned')

                response = jsonify({})
                response.status_code = HTTP_CREATED
            except Exception as e: # pylint: disable=broad-except
                LOGGER.exception('Something went wrong Creating Service {:s}'.format(str(request)))
                response = jsonify({'error': str(e)})
                response.status_code = HTTP_SERVERERROR
        return response
