# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import grpc, logging
from common.tools.client.RetryDecorator import retry, delay_exponential
from compute.proto.compute_pb2_grpc import ComputeServiceStub
from compute.proto.context_pb2 import (
    AuthenticationResult, Empty, Service, ServiceId, ServiceIdList, ServiceStatus, TeraFlowController)

LOGGER = logging.getLogger(__name__)
MAX_RETRIES = 15
DELAY_FUNCTION = delay_exponential(initial=0.01, increment=2.0, maximum=5.0)
RETRY_DECORATOR = retry(max_retries=MAX_RETRIES, delay_function=DELAY_FUNCTION, prepare_method_name='connect')

class ComputeClient:
    def __init__(self, address, port):
        self.endpoint = '{:s}:{:s}'.format(str(address), str(port))
        LOGGER.debug('Creating channel to {:s}...'.format(str(self.endpoint)))
        self.channel = None
        self.stub = None
        self.connect()
        LOGGER.debug('Channel created')

    def connect(self):
        self.channel = grpc.insecure_channel(self.endpoint)
        self.stub = ComputeServiceStub(self.channel)

    def close(self):
        if(self.channel is not None): self.channel.close()
        self.channel = None
        self.stub = None

    @RETRY_DECORATOR
    def CheckCredentials(self, request : TeraFlowController) -> AuthenticationResult:
        LOGGER.debug('CheckCredentials request: {:s}'.format(str(request)))
        response = self.stub.CheckCredentials(request)
        LOGGER.debug('CheckCredentials result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def GetConnectivityServiceStatus(self, request : ServiceId) -> ServiceStatus:
        LOGGER.debug('GetConnectivityServiceStatus request: {:s}'.format(str(request)))
        response = self.stub.GetConnectivityServiceStatus(request)
        LOGGER.debug('GetConnectivityServiceStatus result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def CreateConnectivityService(self, request : Service) -> ServiceId:
        LOGGER.debug('CreateConnectivityService request: {:s}'.format(str(request)))
        response = self.stub.CreateConnectivityService(request)
        LOGGER.debug('CreateConnectivityService result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def EditConnectivityService(self, request : Service) -> ServiceId:
        LOGGER.debug('EditConnectivityService request: {:s}'.format(str(request)))
        response = self.stub.EditConnectivityService(request)
        LOGGER.debug('EditConnectivityService result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def DeleteConnectivityService(self, request : Service) -> Empty:
        LOGGER.debug('DeleteConnectivityService request: {:s}'.format(str(request)))
        response = self.stub.DeleteConnectivityService(request)
        LOGGER.debug('DeleteConnectivityService result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def GetAllActiveConnectivityServices(self, request : Empty) -> ServiceIdList:
        LOGGER.debug('GetAllActiveConnectivityServices request: {:s}'.format(str(request)))
        response = self.stub.GetAllActiveConnectivityServices(request)
        LOGGER.debug('GetAllActiveConnectivityServices result: {:s}'.format(str(response)))
        return response

    @RETRY_DECORATOR
    def ClearAllConnectivityServices(self, request : Empty) -> Empty:
        LOGGER.debug('ClearAllConnectivityServices request: {:s}'.format(str(request)))
        response = self.stub.ClearAllConnectivityServices(request)
        LOGGER.debug('ClearAllConnectivityServices result: {:s}'.format(str(response)))
        return response
