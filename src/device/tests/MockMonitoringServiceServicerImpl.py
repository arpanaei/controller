# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from queue import Queue
from monitoring.proto.context_pb2 import Empty
from monitoring.proto.monitoring_pb2 import Kpi
from monitoring.proto.monitoring_pb2_grpc import MonitoringServiceServicer

LOGGER = logging.getLogger(__name__)

class MockMonitoringServiceServicerImpl(MonitoringServiceServicer):
    def __init__(self, queue_samples : Queue):
        self.queue_samples = queue_samples

    def IncludeKpi(self, request : Kpi, context) -> Empty:
        self.queue_samples.put(request)
        return Empty()
