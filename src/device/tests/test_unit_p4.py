# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import pytest
from device.service.drivers.p4.p4_driver import P4Driver
from .device_p4 import(
        DEVICE_P4_ADDRESS, DEVICE_P4_PORT, DEVICE_P4_ID, DEVICE_P4_NAME,
        DEVICE_P4_VENDOR, DEVICE_P4_HW_VER, DEVICE_P4_SW_VER,
        DEVICE_P4_PIPECONF, DEVICE_P4_WORKERS, DEVICE_P4_GRACE_PERIOD)
from .mock_p4runtime_service import MockP4RuntimeService


@pytest.fixture(scope='session')
def p4runtime_service():
    _service = MockP4RuntimeService(
        address=DEVICE_P4_ADDRESS, port=DEVICE_P4_PORT,
        max_workers=DEVICE_P4_WORKERS,
        grace_period=DEVICE_P4_GRACE_PERIOD)
    _service.start()
    yield _service
    _service.stop()


@pytest.fixture(scope='session')
def device_driverapi_p4():
    _driver = P4Driver(
        address=DEVICE_P4_ADDRESS,
        port=DEVICE_P4_PORT,
        id=DEVICE_P4_ID,
        name=DEVICE_P4_NAME,
        vendor=DEVICE_P4_VENDOR,
        hw_ver=DEVICE_P4_HW_VER,
        sw_ver=DEVICE_P4_SW_VER,
        pipeconf=DEVICE_P4_PIPECONF)
    _driver.Connect()
    yield _driver
    _driver.Disconnect()


def test_device_driverapi_p4_setconfig(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.SetConfig([])
    return


def test_device_driverapi_p4_getconfig(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.GetConfig()
    return


def test_device_driverapi_p4_getresource(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.GetResource("")
    return


def test_device_driverapi_p4_getstate(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.GetState()
    return


def test_device_driverapi_p4_deleteconfig(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.DeleteConfig([])
    return


def test_device_driverapi_p4_subscribe_state(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.SubscribeState([])
    return


def test_device_driverapi_p4_unsubscribe_state(
        p4runtime_service: MockP4RuntimeService,
        device_driverapi_p4: P4Driver):  # pylint: disable=redefined-outer-name
    device_driverapi_p4.UnsubscribeState([])
    return
