# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
from typing import Dict
from common.orm.fields.ForeignKeyField import ForeignKeyField
from common.orm.fields.PrimaryKeyField import PrimaryKeyField
from common.orm.fields.StringField import StringField
from common.orm.model.Model import Model
from .ContextModel import ContextModel

LOGGER = logging.getLogger(__name__)

class TopologyModel(Model):
    pk = PrimaryKeyField()
    context_fk = ForeignKeyField(ContextModel)
    topology_uuid = StringField(required=True, allow_empty=False)

#    def dump_id(self) -> Dict:
#        context_id = ContextModel(self.database, self.context_fk).dump_id()
#        return {
#            'context_id': context_id,
#            'topology_uuid': {'uuid': self.topology_uuid},
#        }

#    def dump(self) -> Dict:
#        result = {'topology_id': self.dump_id()}
#        return result
