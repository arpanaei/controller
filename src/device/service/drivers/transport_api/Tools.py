# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json, logging, requests
from device.service.driver_api._Driver import RESOURCE_ENDPOINTS

LOGGER = logging.getLogger(__name__)


def find_key(resource, key):
    return json.loads(resource[1])[key]


def config_getter(root_url, resource_key, timeout):
    url = '{:s}/restconf/data/tapi-common:context'.format(root_url)
    result = []
    try:
        response = requests.get(url, timeout=timeout)
    except requests.exceptions.Timeout:
        LOGGER.exception('Timeout connecting {:s}'.format(url))
    except Exception as e:  # pylint: disable=broad-except
        LOGGER.exception('Exception retrieving {:s}'.format(resource_key))
        result.append((resource_key, e))
    else:
        context = json.loads(response.content)

        if resource_key == RESOURCE_ENDPOINTS:
            for sip in context['tapi-common:context']['service-interface-point']:
                result.append(
                    ('/endpoints/endpoint[{:s}]'.format(sip['uuid']), {'uuid': sip['uuid'], 'type': '10Gbps'}))

    return result

def create_connectivity_service(
    root_url, timeout, uuid, input_sip, output_sip, direction, capacity_value, capacity_unit, layer_protocol_name,
    layer_protocol_qualifier):

    url = '{:s}/restconf/data/tapi-common:context/tapi-connectivity:connectivity-context'.format(root_url)
    headers = {'content-type': 'application/json'}
    data = {
        'tapi-connectivity:connectivity-service': [
            {
                'uuid': uuid,
                'connectivity-constraint': {
                    'requested-capacity': {
                        'total-size': {
                            'value': capacity_value,
                            'unit': capacity_unit
                        }
                    },
                    'connectivity-direction': direction
                },
                'end-point': [
                    {
                        'service-interface-point': {
                            'service-interface-point-uuid': input_sip
                        },
                        'layer-protocol-name': layer_protocol_name,
                        'layer-protocol-qualifier': layer_protocol_qualifier,
                        'local-id': input_sip
                    },
                    {
                        'service-interface-point': {
                            'service-interface-point-uuid': output_sip
                        },
                        'layer-protocol-name': layer_protocol_name,
                        'layer-protocol-qualifier': layer_protocol_qualifier,
                        'local-id': output_sip
                    }
                ]
            }
        ]
    }
    results = []
    try:
        LOGGER.info('Connectivity service {:s}: {:s}'.format(str(uuid), str(data)))
        response = requests.post(url=url, data=json.dumps(data), timeout=timeout, headers=headers)
        LOGGER.info('TAPI response: {:s}'.format(str(response)))
    except Exception as e:  # pylint: disable=broad-except
        LOGGER.exception('Exception creating ConnectivityService(uuid={:s}, data={:s})'.format(str(uuid), str(data)))
        results.append(e)
    else:
        if response.status_code != 201:
            msg = 'Could not create ConnectivityService(uuid={:s}, data={:s}). status_code={:s} reply={:s}'
            LOGGER.error(msg.format(str(uuid), str(data), str(response.status_code), str(response)))
        results.append(response.status_code == 201)
    return results

def delete_connectivity_service(root_url, timeout, uuid):
    url = '{:s}/restconf/data/tapi-common:context/tapi-connectivity:connectivity-context/connectivity-service={:s}'
    url = url.format(root_url, uuid)
    results = []
    try:
        response = requests.delete(url=url, timeout=timeout)
    except Exception as e:  # pylint: disable=broad-except
        LOGGER.exception('Exception deleting ConnectivityService(uuid={:s})'.format(str(uuid)))
        results.append(e)
    else:
        if response.status_code != 201:
            msg = 'Could not delete ConnectivityService(uuid={:s}). status_code={:s} reply={:s}'
            LOGGER.error(msg.format(str(uuid), str(response.status_code), str(response)))
        results.append(response.status_code == 202)
    return results
