# Copyright 2021-2023 H2020 TeraFlow (https://www.teraflow-h2020.eu/)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


NAMESPACE_NETCONF       = 'urn:ietf:params:xml:ns:netconf:base:1.0'

NAMESPACE_INTERFACES             = 'http://openconfig.net/yang/interfaces'
NAMESPACE_INTERFACES_IP          = 'http://openconfig.net/yang/interfaces/ip'
NAMESPACE_NETWORK_INSTANCE       = 'http://openconfig.net/yang/network-instance'
NAMESPACE_NETWORK_INSTANCE_TYPES = 'http://openconfig.net/yang/network-instance-types'
NAMESPACE_OPENCONFIG_TYPES       = 'http://openconfig.net/yang/openconfig-types'
NAMESPACE_PLATFORM               = 'http://openconfig.net/yang/platform'
NAMESPACE_PLATFORM_PORT          = 'http://openconfig.net/yang/platform/port'
NAMESPACE_VLAN                   = 'http://openconfig.net/yang/vlan'

NAMESPACES = {
    'nc'   : NAMESPACE_NETCONF,
    'oci'  : NAMESPACE_INTERFACES,
    'ociip': NAMESPACE_INTERFACES_IP,
    'ocni' : NAMESPACE_NETWORK_INSTANCE,
    'ocnit': NAMESPACE_NETWORK_INSTANCE_TYPES,
    'ococt': NAMESPACE_OPENCONFIG_TYPES,
    'ocp'  : NAMESPACE_PLATFORM,
    'ocpp' : NAMESPACE_PLATFORM_PORT,
    'ocv'  : NAMESPACE_VLAN,
}
